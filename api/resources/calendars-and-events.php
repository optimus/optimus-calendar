<?php
$get = function ()
{
	global $connection, $resource, $input;
	auth();
	allowed_origins_only();

	$owner = check('owner', $input->path[1], 'strictly_positive_integer', true);

	include('resources/calendars.php');
	$calendars = sanitize($resource, $get());

	include('resources/calendars_events.php');
	foreach($calendars['data'] as &$calendar)
	{
		$input->path = ['optimus-calendar', $calendar['owner']['id'] ?? $owner, 'calendars', $calendar['owner']['calendar'] ?? $calendar['id'], 'events'];
		unset($input->id);
		$calendar['events'] = $get()['data'];
	}
	return array("code" => 200, "data" => sanitize($resource, $calendars['data']));
};
?>