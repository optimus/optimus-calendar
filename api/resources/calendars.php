<?php
include_once 'libs/datatables.php';
$resource = json_decode('
{
	"id": { "type": "strictly_positive_integer", "field": "calendars.id", "post": ["ignored"], "patch": ["immutable"], "default": 0},
	"server": { "type": "domain", "field": "calendars.server", "post": ["ignored"], "patch": ["immutable"]},
	"user": { "type": "strictly_positive_integer", "field": "calendars.user", "post": ["ignored"], "patch": ["immutable"]},
	"calendar": { "type": "strictly_positive_integer", "field": "calendars.calendar", "post": ["ignored"], "patch": ["immutable"]},
	"name": { "type": "string", "field": "calendars.name", "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "patch": ["notnull", "notempty"]},
	"color": { "type": "hexcolor", "field": "calendars.color", "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "patch": ["notnull", "notempty"], "default": "485fc7"},
	"display": { "type": "boolean", "field": "calendars.display", "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "patch": ["notnull", "notempty"], "default": 1},
	"displayname" : { "type" : "string", "field": "calendars.displayname", "virtual": true }
}
', null, 512, JSON_THROW_ON_ERROR);


$get = function ()
{
	global $connection, $resource, $input;
	auth();
	allowed_origins_only();
		
	$input->id = check('id', $input->path[3], 'strictly_positive_integer', false);
	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);
	$server = 'api.' . getenv('DOMAIN');

	//VERIFIE LES PARTAGES EXISTANTS, CREE LES SOUSCRIPTIONS MANQUANTES, SUPPRIME LES SOUSCRIPTIONS OBSOLETES
	if ($_GET['subscriptions'] == 'true')
	{
		$sharers = $connection->query('SELECT DISTINCT owner as id FROM server.authorizations WHERE resource LIKE "calendars%" AND user = ' . $input->owner)->fetchAll(PDO::FETCH_OBJ);
		foreach($sharers as $sharer)
		{
			$sharer_calendars = $connection->query('SELECT id, name, color FROM user_' . $sharer->id . '.calendars WHERE user IS NULL')->fetchAll(PDO::FETCH_OBJ);
			foreach($sharer_calendars as $sharer_calendar)
			{
				$restrictions = get_restrictions($input->user->id, $sharer->id, 'calendars/' . $sharer_calendar->id);
				if (in_array('read', $restrictions))
				{
					$delete_subscription = $connection->prepare('DELETE FROM user_' . $input->owner . '.calendars WHERE server=:server AND user=:user AND calendar=:calendar');
					$delete_subscription->bindParam('server', $server, PDO::PARAM_STR);
					$delete_subscription->bindParam('user', $sharer->id, PDO::PARAM_INT);
					$delete_subscription->bindParam('calendar', $sharer_calendar->id, PDO::PARAM_INT);
					if (!$delete_subscription->execute())
						return array("code" => 500, "message" => $delete_subscription->errorInfo()[2]);
				}
				else
				{
					$check_for_subscription = $connection->query('SELECT id FROM user_' . $input->owner . '.calendars WHERE server IS NOT NULL AND user = ' . $sharer->id . ' AND calendar = ' . $sharer_calendar->id)->fetchAll(PDO::FETCH_OBJ);
					if (sizeof($check_for_subscription) == 0)
					{
						$create_subscription = $connection->prepare('INSERT INTO user_' . $input->owner . '.calendars SET server=:server, user=:user, calendar=:calendar, name=:name, color=:color');
						$create_subscription->bindParam('server', $server, PDO::PARAM_STR);
						$create_subscription->bindParam('user', $sharer->id, PDO::PARAM_INT);
						$create_subscription->bindParam('calendar', $sharer_calendar->id, PDO::PARAM_INT);
						$create_subscription->bindParam('name', $sharer_calendar->name, PDO::PARAM_STR);
						$create_subscription->bindParam('color', $sharer_calendar->color, PDO::PARAM_STR);
						if (!$create_subscription->execute())
							return array("code" => 500, "message" => $create_subscription->errorInfo()[2]);
					}
				}
			}
		}
	}

	if (isset($input->id))
		$input->body->filter[] = (object)array("field"=> "id", "type" => "=", "value" => $input->id);
	else if ($_GET['subscriptions'] != 'true' OR $input->user->id != $input->owner)
		$input->body->filter[] = (object)array("field"=> "user", "type" => "isnull");

	$results = datatable_request($connection, $resource, 'user_' . $input->owner, 'calendars', $restrictions);

	if (isset($input->id) AND sizeof($results) == 0)
		return array("code" => 404, "message" => "Cet agenda n'existe pas");

	for ($i=0; $i < sizeof($results); $i++)
	{
		if ($results[$i]['user'] != '')
		{
			$results[$i]['owner']['id'] = (int)$results[$i]['user'];
			$results[$i]['owner']['server'] = $results[$i]['server'];
			$results[$i]['owner']['calendar'] = (int)$results[$i]['calendar'];
			$owner = $connection->query('SELECT displayname FROM server.users WHERE id = "' . $results[$i]['owner']['id'] . '"')->fetchObject();
			$results[$i]['owner']['displayname'] = $owner->displayname;
			if (!is_admin($input->user->id))
				$results[$i]['restrictions'] =  get_restrictions($input->user->id, $results[$i]['user'], 'calendars/' . $results[$i]['calendar']);
			else
				$results[$i]['restrictions'] = array();
		}
		else if ($input->user->id != $input->owner AND !is_admin($input->user->id))
			$results[$i]['restrictions'] =  get_restrictions($input->user->id, $input->owner, 'calendars/' . $results[$i]['calendar']);
		else
			$results[$i]['restrictions'] = array();
			
		unset($results[$i]['server']);
		unset($results[$i]['user']);
		unset($results[$i]['calendar']);
	}
		
	$results = array_values(array_filter($results, fn ($result) => (!in_array('read', $result['restrictions']))));
		
	return array("code" => 200, "data" => sanitize($resource, $results));
};


$post = function ()
{
	global $connection, $resource, $input;
	auth();
	allowed_origins_only();

	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);
	
	check_input_body($resource, 'post');
	
	$restrictions = get_restrictions($input->user->id, $input->owner, 'calendars');
	if (in_array('create', $restrictions))
		return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour créer un agenda chez cet utilisateur");

	$query = datatables_insert($connection, $resource, 'user_' . $input->owner, 'calendars');

	if($query->execute())
	{
		$input->body->filter[] = (object)array("field"=> "id", "type" => "=", "value" => $connection->lastInsertId());
		$results = datatable_request($connection, $resource, 'user_' . $input->owner, 'calendars');
		unset($results[0]['server']);
		unset($results[0]['user']);
		unset($results[0]['calendar']);
		return array("code" => 201, "data" => sanitize($resource, array_merge($results[0], ['restrictions' => $restrictions])));
	}
	else
		return array("code" => 500, "message" => $query->errorInfo()[2]);
};


$patch = function ()
{
	global $connection, $resource, $input;
	auth();
	allowed_origins_only();
	
	$input->id = check('id', $input->path[3], 'strictly_positive_integer', true);
	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);
	
	check_input_body($resource, 'patch');
	
	if (count(array_intersect(array_keys((array)$input->body),array_keys(array_filter((array)$resource, function($item){return !in_array('immutable', (array)$item->patch);})))) == 0) 
		return array("code" => 400, "message" => "Aucune donnée à modifier n'a été transmise");

	$restrictions = get_restrictions($input->user->id, $input->owner, 'calendars/' . $input->id);
	if (in_array('write', $restrictions))
		return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour modifier cet agenda");
	
	$exists = $connection->query("SELECT id, `server` FROM `user_" . $input->owner . "`.`calendars` WHERE id = '" . $input->id . "'");
	if ($exists->rowCount() == 0)
		return array("code" => 404, "message" => "Cet agenda n'existe pas");

	$query = datatables_update($connection, $resource,'user_' . $input->owner, 'calendars', $input->id);
	
	if($query->execute())
	{
		$input->body->filter[] = (object)array("field"=> "id", "type" => "=", "value" => $input->id);
		$results = datatable_request($connection, $resource, 'user_' . $input->owner, 'calendars');
		unset($results[0]['server']);
		unset($results[0]['user']);
		unset($results[0]['calendar']);
		return array("code" => 200, "data" => sanitize($resource, array_merge($results[0], ['restrictions' => $restrictions])));
	}
};


$delete = function ()
{
	global $connection, $input;
	auth();
	allowed_origins_only();
	
	$input->id = check('id', $input->path[3], 'strictly_positive_integer', true);
	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);

	$restrictions = get_restrictions($input->user->id, $input->owner, 'calendars/' . $input->id);
	if (in_array('delete', $restrictions))
		return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour supprimer cet agenda");

	$exists = $connection->query("SELECT * FROM `user_" . $input->owner . "`.`calendars` WHERE id = '" . $input->id . "'");
	if ($exists->rowCount() == 0)
		return array("code" => 404, "message" => "Cet agenda n'existe pas");
	$calendar = $exists->fetchObject();

	if ($calendar->user)
		return array("code" => 400, "message" => "Cet agenda ne peut pas être supprimé car il appartient à un autre utilisateur.");

	if (!$calendar->user)
	{
		$calendar_delete = $connection->query("DELETE FROM `user_" . $input->owner . "`.`calendars` WHERE id = '" . $input->id . "'");
		$authorization_delete = $connection->query("DELETE FROM `server`.`authorizations` WHERE owner = '" . $input->owner . "' AND resource = 'calendars/" . $input->id . "'");
	}
	
	return array("code" => 200);
};
?>
