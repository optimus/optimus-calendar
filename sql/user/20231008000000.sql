CREATE TABLE IF NOT EXISTS `calendars` (
	`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`server` varchar(128) DEFAULT NULL,
	`user` int(10) unsigned DEFAULT NULL,
	`calendar` int(10) unsigned DEFAULT NULL,
	`name` varchar(64) DEFAULT NULL,
	`color` varchar(6) DEFAULT 'ff6384',
	`display` bit(1) NOT NULL DEFAULT b'1',
	`displayname` varchar(128) GENERATED ALWAYS AS (coalesce(nullif(ifnull(`name`,''),''),concat('Agenda N°', id))) VIRTUAL,
	PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;

INSERT IGNORE INTO `calendars` SET `id` = 1, `name` = 'Agenda principal';

CREATE TABLE IF NOT EXISTS `calendars_events` (
	`id` int unsigned NOT NULL AUTO_INCREMENT,
	`calendar` int unsigned DEFAULT NULL,
	`title` varchar(64) DEFAULT NULL,
	`notes` text DEFAULT NULL,
	`start` datetime DEFAULT NULL,
	`end` datetime DEFAULT NULL,
	`allday` bit NOT NULL DEFAULT b'0',
	`rrule` text DEFAULT NULL,
	`vevent` text DEFAULT NULL,
	`displayname` varchar(128) GENERATED ALWAYS AS (coalesce(nullif(ifnull(`title`,''),''),concat('Evènement N°', id))) VIRTUAL,
	PRIMARY KEY (`id`) USING BTREE,
	KEY `calendars_to_events` (`calendar`),
	CONSTRAINT `calendars_to_events` FOREIGN KEY (`calendar`) REFERENCES `calendars` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `calendars_events_exdates` (
	`id` int unsigned NOT NULL AUTO_INCREMENT,
	`event` int unsigned DEFAULT NULL,
	`date` datetime DEFAULT NULL,
	PRIMARY KEY (`id`) USING BTREE,
	KEY `events_to_exdates` (`event`),
	CONSTRAINT `events_to_exdates` FOREIGN KEY (`event`) REFERENCES `calendars_events` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `calendars_events_properties` (
	`id` int unsigned NOT NULL AUTO_INCREMENT,
	`event` int unsigned DEFAULT NULL,
	`property` varchar(32) DEFAULT NULL,
	`value` varchar(255) DEFAULT NULL,
	PRIMARY KEY (`id`) USING BTREE,
	KEY `events_to_properties` (`event`),
	CONSTRAINT `events_to_properties` FOREIGN KEY (`event`) REFERENCES `calendars_events` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;