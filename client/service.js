export default class OptimusCalendarService
{
	constructor()
	{
		this.name = 'optimus-calendar'
		this.resources = [
			{
				id: 'calendars',
				name: 'agendas',
				description: 'Tous les agendas',
				path: 'calendars'
			},
			{
				id: 'calendar',
				name: 'agenda',
				description: 'Un agenda déterminé',
				path: 'calendars/id',
				endpoint: '/optimus-calendar/{owner}/calendars'
			}
		]
		this.swagger_modules =
			[
				{
					id: "optimus-calendar",
					title: "OPTIMUS CALENDAR",
					path: "/services/optimus-calendar/optimus-calendar.yaml",
					filters: ["resources", "calendars", "subscriptions", "service"]
				}
			]
		store.services.push(this)
	}

	login()
	{
		leftmenu.create('optimus-cloud', 'CLOUD')
		leftmenu['optimus-cloud'].add('calendars', 'Agendas', 'fas fa-calendar', () => router('optimus-calendar/calendars'))

		store.resources = store.resources.concat(this.resources)
	}

	logout()
	{
		leftmenu['optimus-cloud'].querySelector('#calendars').remove()
		for (let resource of this.resources)
			store.resources.splice(store.resources.findIndex(item => item.id == resource.id), 1)
		store.services = store.services.filter(service => service.name != this.id)
	}

	async global_search(search_query)
	{
		let results = new Array()
		let calendars = await rest(store.user.server + '/optimus-calendar/' + store.user.id + '/calendars')

		for (let calendar of calendars.data)
		{
			results.push
				(
					{
						icon: 'fas fa-circle',
						icon_color: '#' + calendar.color,
						title: "Agenda (" + calendar.name + ")",
						data: rest((calendar.owner ? calendar.owner.server : store.user.server) + '/optimus-calendar/' + (calendar.owner ? calendar.owner.id : store.user.id) + '/calendars/' + (calendar.owner ? calendar.owner.calendar : calendar.id) + '/events', 'GET', { search: search_query })
							.then(events => events?.data?.map(event => { return { displayname: event.title, route: 'optimus-calendar/calendars?date=' + event.start.slice(0, 10) + '&event=' + event.id } }))
					}
				)
		}
		return results
	}

	dashboard()
	{
		//if (!document.getElementById('quickaction-cloud-header'))
		//quickactions_add('quickaction-cloud-header', 'header', 'CLOUD')
		//quickactions_add('quickaction-cloud-event-create', 'item', 'Créer un nouvel évènement', 'fas fa-calendar-plus', () => quickactionEventCreate())
		//fetch('/services/optimus-calendar/dashboard.html').then(response => response.text()).then(response => document.getElementById('dashboard').insertAdjacentHTML('beforeend', response))
	}
}