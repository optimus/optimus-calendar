export default class editCalendarEvent
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
		this.server = store.queryParams.server || store.user.server
		this.owner = store.queryParams.owner || store.user.id
	}

	async init()
	{
		await load('/services/optimus-calendar/calendars/editEvent.html', this.target)

		await load_script('/services/optimus-calendar/calendars/calendarsTabs.js', modal)

		const info = this.params[0]

		const calendarInstance = this.params[1]

		const currentYear = new Date().getUTCFullYear() + 3

		document.getElementById('repeat-end-date-selector').setAttribute('max', currentYear + '-12-31')

		const toISOStringWithTimezone = date =>
		{
			const tzOffset = -date.getTimezoneOffset()
			const diff = tzOffset >= 0 ? '+' : '-'
			const pad = n => `${Math.floor(Math.abs(n))}`.padStart(2, '0')
			return date.getFullYear() +
				'-' + pad(date.getMonth() + 1) +
				'-' + pad(date.getDate()) +
				'T' + pad(date.getHours()) +
				':' + pad(date.getMinutes()) +
				':' + pad(date.getSeconds()) +
				diff + pad(tzOffset / 60) +
				':' + pad(tzOffset % 60)
		}


		if (info.event._def.recurringDef)
		{
			var rule = new rrule.RRule(info.event._def.recurringDef.typeData.rruleSet._rrule[0].origOptions)

			const allOccs = rule.all()

			const movedOcc = allOccs.find(el => el.toISOString().slice(0, 10) === info.event.start.toISOString().slice(0, 10))

			const movedOccPosition = allOccs.indexOf(movedOcc)

			var passedCount = movedOccPosition

			var remainingCount = allOccs.length - passedCount

			var before = rule.before(info.event.start)

		}

		// ****modification d'événement****

		//** 1. Récupération des calendriers et souscriptions, puis récupération des restrictions de chaque calendrier */

		rest(`${store.user.server}/optimus-calendar/${store.user.id}/calendars?subscriptions=true`, 'GET', {}).then((data) =>
		{
			const calendars = data.data

			const selector = document.getElementById('calendar-select')

			const createOptions = async () =>
			{
				for (let cal of calendars)
				{

					if (cal.owner)
					{
						//* Propriété owner existante = il s'agit d'une souscription - on vérifie si autorisation de lecture au minimum avant de l'afficher

						const auth = await rest(`${cal.owner.server}/optimus-base/authorizations`, 'GET', { user: store.user.id, owner: cal.owner.id, resource: 'calendars/' + cal.owner.calendar }).then((data) =>
						{
							const auth = data.data[0]
							return auth
						})

						if (auth && auth.read === '0')
						{
						}
						else if (auth)
						{
							await rest(`${cal.owner.server}/optimus-calendar/${cal.owner.id}/calendars/${cal.owner.calendar}`, 'GET', {}).then((data) =>
							{
								if (data.data)
								{
									if (data.data.restrictions) cal.restrictions = data.data.restrictions

									let option = document.createElement('option')
									option.value = cal.id
									option.text = cal.displayname
									option.dataset.server = cal.owner ? cal.owner.server : store.user.server
									option.dataset.owner = cal.owner ? cal.owner.id : store.user.id
									option.dataset.calendar = cal.owner ? cal.owner.calendar : cal.id
									selector.appendChild(option)
								}
							})
						}
					} else
					{
						let option = document.createElement('option')
						option.value = cal.id
						option.text = cal.displayname
						option.dataset.server = cal.owner ? cal.owner.server : store.user.server
						option.dataset.owner = cal.owner ? cal.owner.id : store.user.id
						option.dataset.calendar = cal.owner ? cal.owner.calendar : cal.id
						selector.appendChild(option)
					}
				}
			}

			createOptions().then(async () =>
			{

				//Après récupération des calendriers en tant qu'options, fixation de l'état de l'event à l'ouverture

				const el = document.getElementById('calendar-select')

				let eventState = {
					server: el.options[el.selectedIndex].getAttribute('data-server'),
					owner: el.options[el.selectedIndex].getAttribute('data-owner'),
					calendarId: el.options[el.selectedIndex].getAttribute('data-calendar')
				}

				//Création de l'objet eventExtendedProperties dont les propriétés seront crées par chaque module

				//let eventExtendedProperties = {}

				//Ajout des tabs prévus par chaque module dans son fichier service.js
				for (const service of store.services)
					if (service.optimus_calendar_event_editor_tabs)
						for (let tab of service.optimus_calendar_event_editor_tabs)
						{
							//AJOUT D'UN TAB
							let li = document.createElement('li')
							li.dataset.target = tab.id
							li.innerHTML = '<a>' + tab.text + '</a>'
							document.getElementById('event-tabs-list').appendChild(li)

							//AJOUT DU CONTENU DU TAB
							let tab_content = document.createElement('div')
							tab_content.id = tab.id
							tab_content.classList.add('is-hidden')
							document.getElementById('tab-content').appendChild(tab_content)
							await load(tab.link, null, info)
						}

				//activation des tabs

				activateCalendarsTabs()

				// modif de l'état en cas de sélection d'un nouveau calendrier

				el.addEventListener('change', () =>
				{
					eventState.server = el.options[el.selectedIndex].getAttribute('data-server')
					eventState.owner = el.options[el.selectedIndex].getAttribute('data-owner')
					eventState.calendarId = el.options[el.selectedIndex].getAttribute('data-calendar')
				})

				//** event listener sur le select

				selector.addEventListener('change', (evt) =>
				{
					const selectedCal = calendars.find((el) => el.id == evt.target.selectedOptions[0].value)

					const form = document.getElementById('edit-event-form')

					form.addEventListener('keypress', function (e)
					{
						if (e.key === 'Enter')
						{
							e.preventDefault()
						}
					})

					const saveButton = document.getElementById('save-changes')
					const delButton = document.getElementById('delete-event')

					if (selectedCal.owner)
					{
						if (selectedCal.restrictions?.includes('write'))
						{
							for (const el of form.elements)
							{
								if (el.tagName.toLowerCase() === 'input' || el.tagName.toLowerCase() === 'textarea' || (el.tagName.toLowerCase() === 'select' && el.id !== 'calendar-select'))
								{
									el.setAttribute('disabled', '')
									saveButton.setAttribute('disabled', '')
								}
							}
						} else
						{
							for (const el of form.elements)
							{
								if (el.tagName.toLowerCase() === 'input' || el.tagName.toLowerCase() === 'textarea' || (el.tagName.toLowerCase() === 'select' && el.id !== 'calendar-select'))
								{
									el.removeAttribute('disabled')
									saveButton.removeAttribute('disabled', '')
								}
							}
						}

						if (selectedCal.restrictions?.includes('delete'))
						{
							delButton.setAttribute('disabled', '')
						} else
						{
							delButton.removeAttribute('disabled', '')
						}
					} else
					{
						for (const el of form.elements)
						{
							el.removeAttribute('disabled')
							saveButton.removeAttribute('disabled', '')
							delButton.removeAttribute('disabled', '')
						}
					}
				})

				//** Séléctionner automatiquement la bonne valeur dans le select */
				const optionsArray = Array.from(selector.options)
				const found = optionsArray.find((el) => el.text === info.event.extendedProps.calName)
				selector.value = found.value
				selector.dispatchEvent(new Event('change'))

				//** Event listener sur start,end et untilEnd (sous forme de fonction afin de la relancer lorsqu'on clone le template) */

				const debounce = (func, wait, immediate) =>
				{
					var timeout

					return function executedFunction()
					{
						var context = this
						var args = arguments

						var later = function ()
						{
							timeout = null
							if (!immediate) func.apply(context, args)
						}

						var callNow = immediate && !timeout

						clearTimeout(timeout)

						timeout = setTimeout(later, wait)

						if (callNow) func.apply(context, args)
					}
				}

				const startEndListen = () =>
				{
					document.getElementById('start').addEventListener(
						'change',
						debounce((evt) =>
						{
							if (evt.target.value === '')
							{
								optimusToast('Veuillez saisir une date complète', 'is-warning')
								document.getElementById('start').classList.add('is-danger', 'is-active')
								document.getElementById('save-changes').setAttribute('disabled', '')
							} else
							{
								document.getElementById('start').classList.remove('is-danger', 'is-active')
								document.getElementById('save-changes').removeAttribute('disabled', '')
							}

							// //* Modification automatique de l'heure de fin si l'heure de début fixée est ultérieure

							if (new Date(evt.target.value) >= new Date(document.getElementById('end').value))
							{
								const startTime = new Date(evt.target.value)
								if (document.getElementById('allday-checkbox-change-event').checked === false)
								{
									startTime.setHours(startTime.getHours() + 1)
								}
								document.getElementById('end').value = document.getElementById('allday-checkbox-change-event').checked ? (startTime.toLocaleString('sv').replace(' ', 'T').slice(0, 10)) : (startTime.toLocaleString('sv').replace(' ', 'T').slice(0, 19))
							}

							//**Modif automatique de la date de fin de répétition (until) si la date de début de l'event est fixée ultérieurement */
							if (new Date(evt.target.value) >= new Date(document.getElementById('repeat-end-date-selector').value))
							{
								const endDate = new Date(evt.target.value)
								endDate.setDate(endDate.getUTCDate() + 1)
								document.getElementById('repeat-end-date-selector').value = endDate.toISOString().slice(0, 10)
							}

						}, 250)
					)

					document.getElementById('end').addEventListener(
						'change',
						debounce((evt) =>
						{
							if (evt.target.value === '')
							{
								optimusToast('Veuillez saisir une date complète', 'is-warning')
								document.getElementById('end').classList.add('is-danger', 'is-active')
								document.getElementById('save-changes').setAttribute('disabled', '')
							} else
							{
								document.getElementById('end').classList.remove('is-danger', 'is-active')
								document.getElementById('save-changes').removeAttribute('disabled', '')
							}

							// //* Modification automatique de l'heure de début si l'heure de fin fixée est antérieure

							if (new Date(evt.target.value) <= new Date(document.getElementById('start').value))
							{
								const startTime = new Date(evt.target.value)
								if (document.getElementById('allday-checkbox-change-event').checked === false)
								{
									startTime.setHours(startTime.getHours() - 1)
								}
								document.getElementById('start').value = document.getElementById('allday-checkbox-change-event').checked ? startTime.toLocaleString('sv').replace(' ', 'T').slice(0, 10) : startTime.toLocaleString('sv').replace(' ', 'T').slice(0, 19)
							}

							if (info.event._def.recurringDef.typeData.rruleSet._rrule[0].origOptions.until)
							{
								const event = new Event('change')
								document.getElementById('until-option').dispatchEvent(event)
							}

							//**Modif automatique de la date de fin de répétition (until) si la date de fin de l'event est fixée ultérieurement */
							if (new Date(evt.target.value) >= new Date(document.getElementById('repeat-end-date-selector').value))
							{
								const endDate = new Date(evt.target.value)
								endDate.setDate(endDate.getUTCDate() + 1)
								document.getElementById('repeat-end-date-selector').value = endDate.toISOString().slice(0, 10)
							}

						}, 250)
					)

				}

				startEndListen()

				//** event listener sur le checkbox allDay */

				document.getElementById('allday-checkbox-change-event').addEventListener('change', () =>
				{
					document.getElementById('start').type = document.getElementById('allday-checkbox-change-event').checked ? 'date' : 'datetime-local'
					document.getElementById('end').type = document.getElementById('allday-checkbox-change-event').checked ? 'date' : 'datetime-local'

					if (document.getElementById('allday-checkbox-change-event').checked)
					{
						document.getElementById('start').value = info.event.startStr.slice(0, 10)
						document.getElementById('end').value = info.event.endStr.slice(0, 10)
						startEndListen()
					} else
					{
						if (info.event.allDay)
						{
							document.getElementById('start').value = info.event.startStr + ' 08:00:00'
							//*? Note : fullcalendar considère qu'un event dont le param end est le 3 se termine en fait le 2 (end est exclusif) - pour l'affichage on doit donc retirer 24h ici, car on a passé dans index.js une valeur end modifiée pour l'affichage sur le calendrier (+24h) */
							const veille = new Date(new Date(info.event.endStr).getTime() - 60 * 60 * 24 * 1000)
							document.getElementById('end').value = veille.toISOString().slice(0, 10) + ' 09:00:00'
						} else
						{
							document.getElementById('start').value = info.event.startStr.slice(0, 19)
							document.getElementById('end').value = info.event.endStr.slice(0, 19)
						}
						startEndListen()
					}

					if (info.event._def.recurringDef.typeData.rruleSet._rrule[0].origOptions.until)
					{
						const event = new Event('change')
						document.getElementById('until-option').dispatchEvent(event)
					}
				})

				if (!store.touchscreen)
				{
					window.setTimeout(function ()
					{
						document.getElementById('change-title').focus()
					}, 0)
				}

				//** Remplissage des champs date */

				document.getElementById('start').type = info.event.allDay ? 'date' : 'datetime-local'
				document.getElementById('end').type = info.event.allDay ? 'date' : 'datetime-local'

				document.getElementById('start').value = info.event.allDay ? info.event.startStr.slice(0, 10) : info.event.startStr.slice(0, 19)

				const veille = new Date(new Date(info.event.endStr).getTime() - 60 * 60 * 24 * 1000)

				document.getElementById('end').value = info.event.allDay ? veille.toISOString().slice(0, 10) : info.event.endStr.slice(0, 19)
				startEndListen()

				// document.getElementById('mini-loader-container').style.display = 'none'
				// document.getElementById('mini-loader').style.display = 'none'
				// document.getElementById('card-body').style.display = 'block'

				// *****Réglages de répétition = RRULE*******

				const options = {
					weekday: 'long',
					year: 'numeric',
					month: 'long',
					day: 'numeric'
				}

				const dayName = info.event.start.toLocaleDateString('fr-FR', options).split(' ')[0]

				const dayNumber = info.event.start.toLocaleDateString('fr-FR', options).split(' ')[1]

				const dateMonth = info.event.start.toLocaleDateString('fr-FR', options).split(' ')[1] + ' ' + info.event.start.toLocaleDateString('fr-FR', options).split(' ')[2]

				document.getElementById('repeat-selector').options[2].innerHTML = 'Toutes les semaines le ' + dayName
				document.getElementById('repeat-selector').options[3].innerHTML = 'Tous les mois le ' + dayNumber
				document.getElementById('repeat-selector').options[4].innerHTML = 'Tous les ans le ' + dateMonth

				var rruleData

				document.getElementById('repeat-selector').addEventListener('change', (evt) =>
				{
					if (evt.target.selectedIndex < 1)
					{
						if (rruleData) rruleData = null
						document.getElementById('repeat-block').classList.add('is-hidden')
						document.getElementById('until-option').checked = false
						document.getElementById('count-option').checked = false
					}

					if (evt.target.selectedIndex >= 1)
					{
						rruleData = new Object()
						document.getElementById('repeat-block').classList.remove('is-hidden')
						document.getElementById('count-option').checked = true
						rruleData.count = 2
						const lendemain = new Date(info.event.endStr)
						lendemain.setDate(lendemain.getDate() + 1)
						document.getElementById('repeat-end-date-selector').value = lendemain.toISOString().slice(0, 10)
						document.getElementById('repeat-end-date-selector').setAttribute('disabled', '')
					}

					if (evt.target.selectedIndex === 1)
					{
						rruleData.freq = 'DAILY'
						rruleData.byweekday = null
					}

					if (evt.target.selectedIndex === 2)
					{
						rruleData.freq = 'WEEKLY'
						rruleData.byweekday = null
					}

					if (evt.target.selectedIndex === 3)
					{
						rruleData.freq = 'MONTHLY'
						rruleData.byweekday = null
					}

					if (evt.target.selectedIndex === 4)
					{
						rruleData.freq = 'YEARLY'
						rruleData.byweekday = null
					}

					if (evt.target.selectedIndex === 5)
					{
						rruleData.freq = 'WEEKLY'
						rruleData.byweekday = ['MO', 'TU', 'WE', 'TH', 'FR']
					}
				})

				document.getElementById('until-option').addEventListener('change', (evt) =>
				{
					if (evt.target.checked)
					{
						document.getElementById('count-option').checked = false
						rruleData.count = null
						document.getElementById('repeat-occurrence-selector').setAttribute('disabled', '')
						document.getElementById('repeat-end-date-selector').removeAttribute('disabled', '')
						// const lendemain = new Date(document.getElementById('repeat-end-date-selector').value)
						// lendemain.setDate(lendemain.getDate() + 1)
						// rruleData.until = lendemain.toISOString().slice(0, 10)
						const endDate = new Date(document.getElementById('end').value)

						const untilDate = new Date(document.getElementById('repeat-end-date-selector').value)

						untilDate.setHours(endDate.getHours())

						untilDate.setMinutes(endDate.getMinutes())

						untilDate.setSeconds(endDate.getSeconds())

						rruleData.until = document.getElementById('allday-checkbox-change-event').checked ? untilDate.toISOString().slice(0, 10) : untilDate.toISOString().slice(0, 19) + 'Z'
					}
				})

				document.getElementById('count-option').addEventListener('change', (evt) =>
				{
					if (evt.target.checked)
					{
						document.getElementById('until-option').checked = false
						rruleData.until = null
						document.getElementById('repeat-end-date-selector').setAttribute('disabled', '')
						document.getElementById('repeat-occurrence-selector').removeAttribute('disabled', '')
						rruleData.count = document.getElementById('repeat-occurrence-selector').value
					}
				})

				document.getElementById('repeat-occurrence-selector').addEventListener('change', (evt) =>
				{
					rruleData.count = evt.target.value
					if (evt.target.checkValidity() !== true)
					{
						optimusToast("Le nombre d'occurrences doit être compris entre 2 et 999", 'is-warning')
						evt.target.classList.add('is-danger', 'is-active')
						document.getElementById('save-changes').setAttribute('disabled', '')
					} else
					{
						evt.target.classList.remove('is-danger', 'is-active')
						document.getElementById('save-changes').removeAttribute('disabled', '')
					}
				})

				document.getElementById('repeat-end-date-selector').addEventListener(
					'change',
					debounce((evt) =>
					{
						if (evt.target.value === '')
						{
							optimusToast('Veuillez saisir une date complète', 'is-warning')
							evt.target.classList.add('is-danger', 'is-active')
							document.getElementById('save-changes').setAttribute('disabled', '')
						} else
						{
							evt.target.classList.remove('is-danger', 'is-active')
							document.getElementById('save-changes').removeAttribute('disabled', '')
						}
						if (evt.target.value !== '' && evt.target.checkValidity() !== true)
						{
							optimusToast('Une limite de 5 ans ne peut pas être dépassée', 'is-warning')
							evt.target.classList.add('is-danger', 'is-active')
							document.getElementById('save-changes').setAttribute('disabled', '')
						}

						const deb = new Date(document.getElementById('start').value)
						const endRep = new Date(evt.target.value)

						if (endRep < deb)
						{
							optimusToast("La date de fin de répétition ne peut être inférieure ou égale au début de l'événement", 'is-warning')
							evt.target.classList.add('is-danger', 'is-active')
							document.getElementById('save-changes').setAttribute('disabled', '')
						}

						// const lendemain = new Date(evt.target.value.slice(0, 10))
						// lendemain.setDate(lendemain.getDate() + 1)
						// rruleData.until = lendemain.toISOString().slice(0, 10)
						const endDate = new Date(document.getElementById('end').value)

						const untilDate = new Date(document.getElementById('repeat-end-date-selector').value)

						untilDate.setHours(endDate.getHours())

						untilDate.setMinutes(endDate.getMinutes())

						untilDate.setSeconds(endDate.getSeconds())

						document.getElementById('allday-checkbox-change-event').checked ? rruleData.until = untilDate.toISOString().slice(0, 10) : rruleData.until = untilDate.toISOString().slice(0, 19) + 'Z'

					}, 250)
				)

				const save_rrule = () =>
				{
					if (rruleData)
					{
						const daysArray = rruleData.byweekday
						if (daysArray)
						{
							var weekMap = daysArray.map((el) =>
							{
								return rrule.RRule[el]
							})
						}

						if (isNaN(rruleData.freq))
						{
							rruleData.freq = rrule.RRule[rruleData.freq]
						}

						rruleData.byweekday = weekMap
						const rule = new rrule.RRule(rruleData)
						const correctedString = rule.toString().slice(6)

						if (correctedString.length === 0)
						{
							return null
						} else
						{
							return correctedString
						}
					} else
					{
						return null
					}
				}

				//*Options rrule existantes à l'ouverture

				if (info.event._def.recurringDef != null)
				{
					const eventRec = info.event._def.recurringDef.typeData.rruleSet._rrule[0].origOptions

					//* Attention : syntaxe is not null nécessaire - sinon la condition n'est pas exécutée quand le résultat est 0
					if (eventRec.freq !== null)
					{
						if (eventRec.freq === 3)
						{
							document.getElementById('repeat-selector').selectedIndex = 1
						} else if (eventRec.freq === 2 && typeof eventRec.byweekday === 'undefined')
						{
							document.getElementById('repeat-selector').selectedIndex = 2
						} else if (eventRec.freq === 1)
						{
							document.getElementById('repeat-selector').selectedIndex = 3
						} else if (eventRec.freq === 0)
						{
							document.getElementById('repeat-selector').selectedIndex = 4
						} else if (eventRec.freq === 2 && typeof eventRec.byweekday !== 'undefined')
						{
							document.getElementById('repeat-selector').selectedIndex = 5
						}
						document.getElementById('repeat-selector').dispatchEvent(new Event('change'))
					}

					if (eventRec.count)
					{
						// document.getElementById('repeat-end-selector').selectedIndex = 0
						document.getElementById('count-option').checked = true

						document.getElementById('count-option').dispatchEvent(new Event('change'))

						document.getElementById('repeat-occurrence-selector').value = eventRec.count

						rruleData.count = eventRec.count

						const lendemain = new Date(info.event.endStr)
						lendemain.setDate(lendemain.getDate() + 1)
						document.getElementById('repeat-end-date-selector').value = lendemain.toISOString().slice(0, 10)

						// document.getElementById('repeat-end-date-selector').value = info.event.endStr.slice(0, 10)
					}

					if (eventRec.until)
					{
						rruleData.until = eventRec.until

						document.getElementById('until-option').checked = true

						document.getElementById('until-option').dispatchEvent(new Event('change'))

						// const veille = new Date(new Date(eventRec.until).getTime() - 60 * 60 * 24 * 1000)
						document.getElementById('repeat-end-date-selector').value = toISOStringWithTimezone(eventRec.until).slice(0, 10)
					}
				}

				// ***FIN GESTION RRULE***

				document.getElementById('change-title').value = info.event.title
				document.getElementById('change-description').value = info.event.extendedProps.notes

				const form = document.getElementById('edit-event-form')

				const deleteEvent = document.getElementById('delete-event')
				const cancel = document.getElementById('cancel-changes')

				//** Enregistrement de modifs d'event */

				form.addEventListener('submit', (evt) =>
				{
					evt.preventDefault()

					if (document.getElementById('end').value == '')
					{
						optimusToast('La date de fin doit être fixée', 'is-warning')
						document.getElementById('end').classList.add('is-danger', 'is-active')
					} else
					{
						if (info.event._def.recurringDef !== null)
						{
							const sel = document.getElementById('calendar-select')
							const selected = sel.selectedIndex
							const dataGroup = sel.options[selected].dataset

							const alldayCheck = document.getElementById('allday-checkbox-change-event')

							const editStart = alldayCheck.checked ? document.getElementById('start').value : new Date(document.getElementById('start').value).toISOString().slice(0, 19).replace('T', ' ')

							const lendemain = new Date(new Date(document.getElementById('end').value).getTime() + 60 * 60 * 24 * 1000)
							const lendemainStr = lendemain.toISOString().slice(0, 10)

							const editEnd = alldayCheck.checked ? lendemainStr : new Date(document.getElementById('end').value).toISOString().slice(0, 19).replace('T', ' ')

							const editTitle = document.getElementById('change-title').value
							const editNotes = document.getElementById('change-description').value

							//*L'event était réccurent, mais l'utilisateur veut supprimer toute réccurence, on ne garde donc que l'occurence cliquée sous un nouvel event
							// * - l'ancien event récurrent est supprimé

							if (save_rrule() === null)
							{
								if (dataGroup.calendar !== info.event.extendedProps.calendar.toString() || dataGroup.owner !== info.event.extendedProps.owner.toString())
								{
									rest(`${info.event.extendedProps.server}/optimus-calendar/${info.event.extendedProps.owner}/calendars/${info.event.extendedProps.calendar}/events/${info.event.id}`, 'DELETE', {}).then(() =>
									{
										rest(`${dataGroup.server}/optimus-calendar/${dataGroup.owner}/calendars/${dataGroup.calendar}/events`, 'POST', {
											title: editTitle,
											notes: editNotes,
											start: editStart,
											end: editEnd,
											allday: alldayCheck.checked ? true : false,
											rrule: null,
											properties: (document?.getElementById('file-finder') && document?.getElementById('file-finder')?.dataset?.id != 'null') ? { optimus_avocats_dossier_id: document.getElementById('file-finder').dataset.id } : null
										}).then(() =>
										{
											calendarInstance.refetchEvents()
											modal.close()
										})
									})
								} else
								{
									rest(`${info.event.extendedProps.server}/optimus-calendar/${info.event.extendedProps.owner}/calendars/${info.event.extendedProps.calendar}/events/${info.event.id}`, 'DELETE', {}).then(() =>
									{
										rest(`${dataGroup.server}/optimus-calendar/${dataGroup.owner}/calendars/${dataGroup.calendar}/events`, 'POST', {
											title: editTitle,
											notes: editNotes,
											start: editStart,
											end: editEnd,
											allday: alldayCheck.checked ? true : false,
											rrule: null,
											properties: (document?.getElementById('file-finder') && document?.getElementById('file-finder')?.dataset?.id != 'null') ? { optimus_avocats_dossier_id: document.getElementById('file-finder').dataset.id } : null
										}).then(() =>
										{
											calendarInstance.refetchEvents()
											modal.close()
										})
									})
								}
								return
							}

							//* On propose à l'utilisateur un prompt avec 3 choix max : modifier seulement l'occurence cliquée, l'occurrence cliquée et les suivantes, ou toutes les occurrences

							document.querySelector('.optimus-tabs').classList.add('is-hidden')
							document.getElementById('tab-content').classList.add('is-hidden')
							document.querySelector('.modal-card-foot').classList.add('is-hidden')

							if (document.querySelector('.cancel-edit-container'))
								document.querySelector('.cancel-edit-container').classList.remove('is-hidden')
							else
							{
								const template = document.getElementById('event-edit-prompt')
								const clone = document.importNode(template.content, true)
								document.querySelector('.modal-card-body').appendChild(clone)
							}

							//document.querySelector('.modal-card-body').innerHTML = ''



							//* Si l'utilisateur modifie le type de fréquence (daily, weekly, etc...),
							//* on ne lui laisse que deux solutions : modifier seulement l'occurrence cliquée, ou modifier seulement l'occurence cliquée et les suivantes

							const computedRrule = rrule.RRule.fromString(save_rrule()).origOptions

							const clickedRrule = info.event._def.recurringDef.typeData.rruleSet._rrule[0].origOptions

							if (clickedRrule.freq !== computedRrule.freq)
							{
								document.getElementById('edit-all').classList.add('is-hidden')
							}


							//* Si l'utilisateur clique la première occurrence,
							//* on ne lui laisse que deux solutions : modifier seulement l'occurrence cliquée, ou modifier seulement toutes les occurrences


							if (passedCount === 0)
							{
								document.getElementById('edit-next').classList.add('is-hidden')
							}


							//*****

							document.getElementById('cancel-edit').addEventListener('click', () =>
							{
								document.querySelector('.optimus-tabs').classList.remove('is-hidden')
								document.getElementById('tab-content').classList.remove('is-hidden')
								document.querySelector('.modal-card-foot').classList.remove('is-hidden')
								document.querySelector('.cancel-edit-container').classList.add('is-hidden')
								//calendarInstance.refetchEvents()
								//modal.close()
							})

							document.getElementById('edit-one').addEventListener('click', () =>
							{
								const deletedDate = info.event.allDay ? info.event.startStr : info.event.start.toISOString()

								if (dataGroup.calendar !== info.event.extendedProps.calendar.toString() || dataGroup.owner !== info.event.extendedProps.owner.toString())
								{
									rest(`${info.event.extendedProps.server}/optimus-calendar/${info.event.extendedProps.owner}/calendars/${info.event.extendedProps.calendar}/events/${info.event.id}`, 'PATCH', {
										exdate: deletedDate
									}).then(() =>
									{
										rest(`${dataGroup.server}/optimus-calendar/${dataGroup.owner}/calendars/${dataGroup.calendar}/events`, 'POST', {
											title: editTitle,
											notes: editNotes,
											start: editStart,
											end: editEnd,
											allday: alldayCheck.checked ? true : false,
											rrule: null,
											properties: (document?.getElementById('file-finder') && document?.getElementById('file-finder')?.dataset?.id != 'null') ? { optimus_avocats_dossier_id: document.getElementById('file-finder').dataset.id } : null
										}).then(() =>
										{
											calendarInstance.refetchEvents()
											modal.close()
										})
									})
								} else
								{
									rest(`${dataGroup.server}/optimus-calendar/${dataGroup.owner}/calendars/${dataGroup.calendar}/events/${info.event.id}`, 'PATCH', {
										exdate: deletedDate
									}).then(() =>
									{
										rest(`${dataGroup.server}/optimus-calendar/${dataGroup.owner}/calendars/${dataGroup.calendar}/events`, 'POST', {
											title: editTitle,
											notes: editNotes,
											start: editStart,
											end: editEnd,
											allday: alldayCheck.checked ? true : false,
											rrule: null,
											properties: (document?.getElementById('file-finder') && document?.getElementById('file-finder')?.dataset?.id != 'null') ? { optimus_avocats_dossier_id: document.getElementById('file-finder').dataset.id } : null
										}).then(() =>
										{
											calendarInstance.refetchEvents()
											modal.close()
										})
									})
								}
							})

							document.getElementById('edit-next').addEventListener('click', () =>
							{
								// ** 1) Modifier l'event existant pour réduire until ou count - pour ne garder que les occurrences passées (qui ne doivent pas comprendre la date cliquée !)
								// ** 2) Puis créer un event qui part de la date cliquée avec soit un count = count initial - count passés, soit un until qui correspond à l'ancien

								if (dataGroup.calendar !== info.event.extendedProps.calendar.toString() || dataGroup.owner !== info.event.extendedProps.owner.toString())
								{
									//*Calendrier différent

									if (info.event._def.recurringDef.typeData.rruleSet._rrule[0].origOptions.until)
									{
										rruleData.until = before

										rest(`${info.event.extendedProps.server}/optimus-calendar/${info.event.extendedProps.owner}/calendars/${info.event.extendedProps.calendar}/events/${info.event.id}`, 'PATCH', {
											rrule: save_rrule()
										}).then(() =>
										{
											rruleData.until = info.event._def.recurringDef.typeData.rruleSet._rrule[0].origOptions.until
											rest(`${dataGroup.server}/optimus-calendar/${dataGroup.owner}/calendars/${dataGroup.calendar}/events`, 'POST', {
												title: editTitle,
												notes: editNotes,
												start: editStart,
												end: editEnd,
												allday: alldayCheck.checked ? true : false,
												rrule: save_rrule(),
												properties: (document?.getElementById('file-finder') && document?.getElementById('file-finder')?.dataset?.id != 'null') ? { optimus_avocats_dossier_id: document.getElementById('file-finder').dataset.id } : null
											}).then(() =>
											{
												calendarInstance.refetchEvents()
												modal.close()
												optimusToast('Événement enregistré', 'is-dark')
											})
										})

									} else if (info.event._def.recurringDef.typeData.rruleSet._rrule[0].origOptions.count)
									{
										rruleData.count = passedCount


										rest(`${info.event.extendedProps.server}/optimus-calendar/${info.event.extendedProps.owner}/calendars/${info.event.extendedProps.calendar}/events/${info.event.id}`, 'PATCH', {
											rrule: passedCount === 1 ? null : save_rrule()
										}).then(() =>
										{
											rruleData.count = remainingCount
											rest(`${dataGroup.server}/optimus-calendar/${dataGroup.owner}/calendars/${dataGroup.calendar}/events`, 'POST', {
												title: editTitle,
												notes: editNotes,
												start: editStart,
												end: editEnd,
												allday: alldayCheck.checked ? true : false,
												rrule: save_rrule(),
												properties: (document?.getElementById('file-finder') && document?.getElementById('file-finder')?.dataset?.id != 'null') ? { optimus_avocats_dossier_id: document.getElementById('file-finder').dataset.id } : null
											}).then(() =>
											{
												calendarInstance.refetchEvents()
												modal.close()
											})
										})

									}
								} else
								{
									//*Calendrier identique

									if (info.event._def.recurringDef.typeData.rruleSet._rrule[0].origOptions.until)
									{

										rruleData.until = before

										rest(`${dataGroup.server}/optimus-calendar/${dataGroup.owner}/calendars/${dataGroup.calendar}/events/${info.event.id}`, 'PATCH', {
											rrule: save_rrule()
										}).then(() =>
										{
											rruleData.until = info.event._def.recurringDef.typeData.rruleSet._rrule[0].origOptions.until
											rest(`${dataGroup.server}/optimus-calendar/${dataGroup.owner}/calendars/${dataGroup.calendar}/events`, 'POST', {
												title: editTitle,
												notes: editNotes,
												start: editStart,
												end: editEnd,
												allday: alldayCheck.checked ? true : false,
												rrule: save_rrule(),
												properties: (document?.getElementById('file-finder') && document?.getElementById('file-finder')?.dataset?.id != 'null') ? { optimus_avocats_dossier_id: document.getElementById('file-finder').dataset.id } : null
											}).then(() =>
											{
												calendarInstance.refetchEvents()
												modal.close()
											})
										})



									} else if (info.event._def.recurringDef.typeData.rruleSet._rrule[0].origOptions.count)
									{
										rruleData.count = passedCount

										rest(`${dataGroup.server}/optimus-calendar/${dataGroup.owner}/calendars/${dataGroup.calendar}/events/${info.event.id}`, 'PATCH', {
											rrule: passedCount === 1 ? null : save_rrule()
										}).then(() =>
										{
											rruleData.count = info.event._def.recurringDef.typeData.rruleSet._rrule[0].origOptions.count - passedCount

											rest(`${dataGroup.server}/optimus-calendar/${dataGroup.owner}/calendars/${dataGroup.calendar}/events`, 'POST', {
												title: editTitle,
												notes: editNotes,
												start: editStart,
												end: editEnd,
												allday: alldayCheck.checked ? true : false,
												rrule: save_rrule(),
												properties: (document?.getElementById('file-finder') && document?.getElementById('file-finder')?.dataset?.id != 'null') ? { optimus_avocats_dossier_id: document.getElementById('file-finder').dataset.id } : null
											}).then(() =>
											{
												calendarInstance.refetchEvents()
												modal.close()
											})
										})

									}
								}
							})

							document.getElementById('edit-all').addEventListener('click', () =>
							{
								let allStart

								let allEnd

								if (alldayCheck.checked)
								{
									allStart = info.event.startStr.slice(0, 10)

									if (info.event.allDay)
									{
										allEnd = info.event.startStr.slice(0, 10)
									} else
									{
										const allLendemain = new Date(new Date(info.event.endStr).getTime() + 60 * 60 * 24 * 1000)
										const allLendemainStr = allLendemain.toISOString().slice(0, 10)

										allEnd = allLendemainStr
									}

								} else
								{
									const firstOcc = info.event._def.recurringDef.typeData.rruleSet._rrule[0].origOptions.dtstart

									allStart = firstOcc.toISOString().slice(0, 10) + ' ' + editStart.slice(11, 19)

									allEnd = firstOcc.toISOString().slice(0, 10) + ' ' + editEnd.slice(11, 19)
								}

								if (dataGroup.calendar !== info.event.extendedProps.calendar.toString() || dataGroup.owner !== info.event.extendedProps.owner.toString())
								{
									rest(`${info.event.extendedProps.server}/optimus-calendar/${info.event.extendedProps.owner}/calendars/${info.event.extendedProps.calendar}/events/${info.event.id}`, 'DELETE', {}).then(() =>
									{
										rest(`${dataGroup.server}/optimus-calendar/${dataGroup.owner}/calendars/${dataGroup.calendar}/events`, 'POST', {
											title: editTitle,
											notes: editNotes,
											start: allStart,
											end: allEnd,
											allday: alldayCheck.checked ? true : false,
											rrule: save_rrule(),
											properties: (document?.getElementById('file-finder') && document?.getElementById('file-finder')?.dataset?.id != 'null') ? { optimus_avocats_dossier_id: document.getElementById('file-finder').dataset.id } : null
										}).then(() =>
										{
											calendarInstance.refetchEvents()
											modal.close()
										})
									})
								} else
								{
									rest(`${dataGroup.server}/optimus-calendar/${dataGroup.owner}/calendars/${dataGroup.calendar}/events/${info.event.id}`, 'PATCH', {
										title: editTitle,
										notes: editNotes,
										start: allStart,
										end: allEnd,
										allday: alldayCheck.checked ? true : false,
										rrule: save_rrule(),
										properties: (document?.getElementById('file-finder') && document?.getElementById('file-finder')?.dataset?.id != 'null') ? { optimus_avocats_dossier_id: document.getElementById('file-finder').dataset.id } : null
									}).then(() =>
									{
										calendarInstance.refetchEvents()
										modal.close()
									})
								}
							})
						} else
						{
							//** Event non réccurrent */

							const sel = document.getElementById('calendar-select')
							const selected = sel.selectedIndex
							const dataGroup = sel.options[selected].dataset

							let alldayCheck = document.getElementById('allday-checkbox-change-event')
							let start = alldayCheck.checked ? document.getElementById('start').value : new Date(document.getElementById('start').value).toISOString().slice(0, 19).replace('T', ' ')

							const lendemain = new Date(new Date(document.getElementById('end').value).getTime() + 60 * 60 * 24 * 1000)
							const lendemainStr = lendemain.toISOString().slice(0, 10)

							let end = alldayCheck.checked ? lendemainStr : new Date(document.getElementById('end').value).toISOString().slice(0, 19).replace('T', ' ')

							if (dataGroup.calendar !== info.event.extendedProps.calendar.toString() || dataGroup.owner !== info.event.extendedProps.owner.toString())
							{
								rest(`${info.event.extendedProps.server}/optimus-calendar/${info.event.extendedProps.owner}/calendars/${info.event.extendedProps.calendar}/events/${info.event.id}`, 'DELETE', {}).then(() =>
								{
									rest(`${dataGroup.server}/optimus-calendar/${dataGroup.owner}/calendars/${dataGroup.calendar}/events`, 'POST', {
										title: document.getElementById('change-title').value,
										notes: document.getElementById('change-description').value,
										start: start,
										end: end,
										allday: alldayCheck.checked ? true : false,
										rrule: save_rrule(),
										properties: (document?.getElementById('file-finder') && document?.getElementById('file-finder')?.dataset?.id != 'null') ? { optimus_avocats_dossier_id: document.getElementById('file-finder').dataset.id } : null
									}).then(() =>
									{
										calendarInstance.refetchEvents()
										modal.close()
									})
								})
							} else
							{
								rest(`${dataGroup.server}/optimus-calendar/${dataGroup.owner}/calendars/${dataGroup.calendar}/events/${info.event.id}`, 'PATCH', {
									title: document.getElementById('change-title').value,
									notes: document.getElementById('change-description').value,
									start: start,
									end: end,
									allday: alldayCheck.checked ? true : false,
									rrule: save_rrule(),
									properties: (document?.getElementById('file-finder') && document?.getElementById('file-finder')?.dataset?.id != 'null') ? { optimus_avocats_dossier_id: document.getElementById('file-finder').dataset.id } : null
								}).then(() =>
								{
									calendarInstance.refetchEvents()
									modal.close()
								})
							}
						}
					}
				})

				cancel.addEventListener('click', (evt) =>
				{
					evt.preventDefault()
					calendarInstance.refetchEvents()
					modal.close()
				})

				//** Suppression d'événement */

				deleteEvent.addEventListener('click', (evt) =>
				{
					evt.preventDefault()

					if (info.event._def.recurringDef !== null)
					{
						const sel = document.getElementById('calendar-select')
						const selected = sel.selectedIndex
						const dataGroup = sel.options[selected].dataset

						document.querySelector('.modal-card-title').innerHTML = "Suppression d'événement"
						document.querySelector('.modal-card-foot').innerHTML = ''

						document.querySelector('.modal-card-body').innerHTML = ''
						const template = document.getElementById('event-delete-prompt')
						const clone = document.importNode(template.content, true)
						document.querySelector('.modal-card-body').appendChild(clone)

						document.getElementById('cancel-occurrence-delete').addEventListener('click', () =>
						{
							calendarInstance.refetchEvents()
							modal.close()
						})

						//** Suppression d'une seule occurrence */

						document.getElementById('delete-one').addEventListener('click', () =>
						{
							let deletedDate

							if (info.oldEvent)
							{
								deletedDate = info.oldEvent.allDay ? info.oldEvent.startStr : info.oldEvent.start.toISOString()
							} else
							{
								deletedDate = info.event.allDay ? info.event.startStr : info.event.start.toISOString()
							}

							rest(`${dataGroup.server}/optimus-calendar/${dataGroup.owner}/calendars/${dataGroup.calendar}/events/${info.event.id}`, 'PATCH', {
								exdate: deletedDate
							}).then(() =>
							{
								calendarInstance.refetchEvents()
								modal.close()
							})
						})

						//** Suppression de l'occurrence cliquée et des suivantes */

						document.getElementById('delete-next').addEventListener('click', () =>
						{
							// const rruleToSave = save_rrule()

							if (info.event._def.recurringDef.typeData.rruleSet._rrule[0].origOptions.until)
							{
								rruleData.until = before

								rest(`${dataGroup.server}/optimus-calendar/${dataGroup.owner}/calendars/${dataGroup.calendar}/events/${info.event.id}`, 'PATCH', {
									rrule: save_rrule()
								}).then(() =>
								{
									calendarInstance.refetchEvents()
									modal.close()
								})
							} else if (info.event._def.recurringDef.typeData.rruleSet._rrule[0].origOptions.count)
							{
								rruleData.count = passedCount

								rest(`${dataGroup.server}/optimus-calendar/${dataGroup.owner}/calendars/${dataGroup.calendar}/events/${info.event.id}`, 'PATCH', {
									rrule: save_rrule()
								}).then(() =>
								{
									calendarInstance.refetchEvents()
									modal.close()
								})
							}
						})

						//** Suppression toutes les occurrences */

						document.getElementById('delete-all').addEventListener('click', () =>
						{
							rest(`${dataGroup.server}/optimus-calendar/${dataGroup.owner}/calendars/${dataGroup.calendar}/events/${info.event.id}`, 'DELETE', {}).then(() =>
							{
								calendarInstance.refetchEvents()
								modal.close()
							})
						})
					} else
					{
						const el = document.getElementById('calendar-select')
						rest(`${el.options[el.selectedIndex].getAttribute('data-server')}/optimus-calendar/${el.options[el.selectedIndex].getAttribute('data-owner')}/calendars/${el.options[el.selectedIndex].getAttribute('data-calendar')}/events/${info.event.id}`, 'DELETE', {}).then(() =>
						{
							calendarInstance.refetchEvents()
							modal.close()
						})
					}
				})
			})
		})

	}

}
