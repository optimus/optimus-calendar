export default class newCalendarEventBySelection
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
	}


	async init()
	{
		await load('/services/optimus-calendar/calendars/newEventBySelection.html', this.target)

		await load_script('/services/optimus-calendar/calendars/calendarsTabs.js', modal)

		const dateSelectInfo = this.params[0]

		const calendarInstance = this.params[1]

		if (!dateSelectInfo.startStr) dateSelectInfo.startStr = dateSelectInfo.dateStr

		if (!dateSelectInfo.endStr) dateSelectInfo.endStr = dateSelectInfo.dateStr

		const currentYear = new Date().getUTCFullYear() + 3

		document.getElementById('repeat-end-date-selector').setAttribute('max', currentYear + '-12-31')

		rest(`${store.user.server}/optimus-calendar/${store.user.id}/calendars?subscriptions=true`, 'GET', {}).then((data) =>
		{
			const calendars = data.data
			const selector = document.getElementById('calendar-select')

			const createOptions = async () =>
			{
				for (let cal of calendars)
				{
					if (cal.owner)
					{
						const auth = await rest(`${cal.owner.server}/optimus-base/authorizations`, 'GET', { user: store.user.id, owner: cal.owner.id, resource: 'calendars/' + cal.owner.calendar }).then((data) =>
						{
							const auth = data.data[0]
							return auth
						})

						if (auth && auth.read === '0')
						{
							// if (data.code === 403 && data.message === 'Vous n'avez pas les autorisations suffisantes pour lire cet agenda')
						}
						else if (auth)
						{
							rest(`${cal.owner.server}/optimus-calendar/${cal.owner.id}/calendars/${cal.owner.calendar}`, 'GET', {}).then((data) =>
							{
								if (data)
								{
									let option = document.createElement('option')
									option.value = cal.id
									option.text = cal.displayname
									option.dataset.server = cal.owner ? cal.owner.server : store.user.server
									option.dataset.owner = cal.owner ? cal.owner.id : store.user.id
									option.dataset.calendar = cal.owner ? cal.owner.calendar : cal.id
									selector.appendChild(option)
									if (data.data.restrictions)
									{
										cal.restrictions = data.data.restrictions
										if (cal.restrictions.includes('write'))
										{
											option.setAttribute('disabled', '')
										}
									}
								}
							})
						}
					} else
					{
						let option = document.createElement('option')
						option.value = cal.id
						option.text = cal.displayname
						option.dataset.server = cal.owner ? cal.owner.server : store.user.server
						option.dataset.owner = cal.owner ? cal.owner.id : store.user.id
						option.dataset.calendar = cal.owner ? cal.owner.calendar : cal.id
						selector.appendChild(option)
					}
				}
			}

			createOptions().then(async () =>
			{

				//Après récupération des calendriers en tant qu'options, fixation de l'état de l'event à l'ouverture

				const el = document.getElementById('calendar-select')

				let eventState = {
					server: el.options[el.selectedIndex].getAttribute('data-server'),
					owner: el.options[el.selectedIndex].getAttribute('data-owner'),
					calendarId: el.options[el.selectedIndex].getAttribute('data-calendar')
				}

				//Création de l'objet eventExtendedProperties dont les propriétés seront crées par chaque module

				let eventExtendedProperties = {}

				//if (dateSelectInfo?.property?.optimus_avocats_dossier_id)
				//eventExtendedProperties.optimus_avocats_dossier_id = dateSelectInfo.property.optimus_avocats_dossier_id

				//Ajout des tabs prévus par chaque module dans son fichier service.js
				for (const service of store.services)
					if (service.optimus_calendar_event_editor_tabs)
						for (let tab of service.optimus_calendar_event_editor_tabs)
						{
							//AJOUT D'UN TAB
							let li = document.createElement('li')
							li.dataset.target = tab.id
							li.innerHTML = '<a>' + tab.text + '</a>'
							document.getElementById('event-tabs-list').appendChild(li)

							//AJOUT DU CONTENU DU TAB
							let tab_content = document.createElement('div')
							tab_content.id = tab.id
							tab_content.classList.add('is-hidden')
							document.getElementById('tab-content').appendChild(tab_content)
							await load(tab.link, null, dateSelectInfo)
						}

				//for (const service of store.services)
				//if (service.optimus_calendar_event_tabs)
				//await service.optimus_calendar_event_tabs(eventState, eventExtendedProperties)

				//activation des tabs
				activateCalendarsTabs()

				// modif de l'état en cas de sélection d'un nouveau calendrier

				el.addEventListener('change', () =>
				{
					eventState.server = el.options[el.selectedIndex].getAttribute('data-server')
					eventState.owner = el.options[el.selectedIndex].getAttribute('data-owner')
					eventState.calendarId = el.options[el.selectedIndex].getAttribute('data-calendar')
				})

				//** Event listener sur start et end (sous forme de fonction afin de la relancer lorsqu'on clone le template) */

				const debounce = (func, wait, immediate) =>
				{
					var timeout

					return function executedFunction()
					{
						var context = this
						var args = arguments

						var later = function ()
						{
							timeout = null
							if (!immediate) func.apply(context, args)
						}

						var callNow = immediate && !timeout

						clearTimeout(timeout)

						timeout = setTimeout(later, wait)

						if (callNow) func.apply(context, args)
					}
				}

				const startEndListen = () =>
				{
					document.getElementById('start').addEventListener(
						'change',
						debounce((evt) =>
						{
							if (evt.target.value === '')
							{
								optimusToast('Veuillez saisir une date complète', 'is-warning')
								document.getElementById('start').classList.add('is-danger', 'is-active')
								document.getElementById('save-new-event').setAttribute('disabled', '')
							} else
							{
								document.getElementById('start').classList.remove('is-danger', 'is-active')
								document.getElementById('save-new-event').removeAttribute('disabled', '')
							}

							// //* Modification automatique de l'heure de fin si l'heure de début fixée est ultérieure

							if (new Date(evt.target.value) >= new Date(document.getElementById('end').value))
							{
								const startTime = new Date(evt.target.value)
								if (document.getElementById('allday-checkbox-new-event').checked === false)
								{
									startTime.setHours(startTime.getHours() + 1)
								}
								document.getElementById('allday-checkbox-new-event').checked ? (document.getElementById('end').value = startTime.toLocaleString('sv', { timeZoneName: 'short' }).replace(' ', 'T').slice(0, 10)) : (document.getElementById('end').value = startTime.toLocaleString('sv', { timeZoneName: 'short' }).replace(' ', 'T').slice(0, 19))
							}
						}, 250)
					)

					document.getElementById('end').addEventListener(
						'change',
						debounce((evt) =>
						{
							if (evt.target.value === '')
							{
								optimusToast('Veuillez saisir une date complète', 'is-warning')
								document.getElementById('end').classList.add('is-danger', 'is-active')
								document.getElementById('save-new-event').setAttribute('disabled', '')
							} else
							{
								document.getElementById('end').classList.remove('is-danger', 'is-active')
								document.getElementById('save-new-event').removeAttribute('disabled', '')
							}

							// //* Modification automatique de l'heure de début si l'heure de fin fixée est antérieure

							if (new Date(evt.target.value) <= new Date(document.getElementById('start').value))
							{
								const startTime = new Date(evt.target.value)
								if (document.getElementById('allday-checkbox-new-event').checked === false)
								{
									startTime.setHours(startTime.getHours() - 1)
								}
								document.getElementById('allday-checkbox-new-event').checked ? (document.getElementById('start').value = startTime.toLocaleString('sv', { timeZoneName: 'short' }).replace(' ', 'T').slice(0, 10)) : (document.getElementById('start').value = startTime.toLocaleString('sv', { timeZoneName: 'short' }).replace(' ', 'T').slice(0, 19))
							}
						}, 250)
					)
				}

				startEndListen()

				//** Remplissage des champs date */


				document.getElementById('start').type = dateSelectInfo.allDay ? 'date' : 'datetime-local'
				document.getElementById('end').type = dateSelectInfo.allDay ? 'date' : 'datetime-local'

				document.getElementById('start').value = dateSelectInfo.allDay ? dateSelectInfo.startStr : dateSelectInfo.startStr.slice(0, 19)
				//*? Note : fullcalendar considère qu'un event allDay dont le param end est le 3 se termine en fait le 2 (end est exclusif) - pour l'affichage on doit donc retirer 24h ici, car on a passé dans index.js une valeur end modifiée pour l'affichage sur le calendrier (+24h) */
				const veille = new Date(new Date(dateSelectInfo.endStr).getTime() - 60 * 60 * 24 * 1000)
				document.getElementById('end').value = dateSelectInfo.allDay ? veille.toISOString().slice(0, 10) : dateSelectInfo.endStr.slice(0, 19)
				startEndListen()

				//** event listener sur le checkbox allDay */

				document.getElementById('allday-checkbox-new-event').addEventListener('change', () =>
				{

					document.getElementById('start').type = document.getElementById('allday-checkbox-new-event').checked ? 'date' : 'datetime-local'
					document.getElementById('end').type = document.getElementById('allday-checkbox-new-event').checked ? 'date' : 'datetime-local'

					if (dateSelectInfo.allDay)
					{
						document.getElementById('start').value = document.getElementById('allday-checkbox-new-event').checked ? dateSelectInfo.startStr : dateSelectInfo.startStr + 'T09:00'
						const veille = new Date(new Date(dateSelectInfo.endStr).getTime() - 60 * 60 * 24 * 1000)
						document.getElementById('end').value = document.getElementById('allday-checkbox-new-event').checked ? veille.toISOString().slice(0, 10) : document.getElementById('end').value = veille.toISOString().slice(0, 10) + 'T10:00'
						startEndListen()
					} else
					{
						if (document.getElementById('allday-checkbox-new-event').checked)
						{
							document.getElementById('start').value = dateSelectInfo.startStr.slice(0, 10)
							document.getElementById('end').value = dateSelectInfo.endStr.slice(0, 10)
						} else
						{
							if (dateSelectInfo.startStr.length === 10)
							{
								document.getElementById('start').value = dateSelectInfo.startStr.slice(0, 10) + 'T09:00'
								document.getElementById('end').value = dateSelectInfo.endStr.slice(0, 10) + 'T10:00'
							} else
							{
								document.getElementById('start').value = dateSelectInfo.startStr.slice(0, 19)
								document.getElementById('end').value = dateSelectInfo.endStr.slice(0, 19)
							}
						}
						startEndListen()
					}
				})

				if (!store.touchscreen)
				{
					window.setTimeout(function ()
					{
						if (dateSelectInfo.title)
							document.getElementById('new-event-title').value = dateSelectInfo.title
						document.getElementById('new-event-title').focus()
					}, 0)
				}

				document.getElementById('allday-checkbox-new-event').checked = dateSelectInfo.allDay ? true : false


				// *****Réglages de répétition = RRULE*******

				const options = {
					weekday: 'long',
					year: 'numeric',
					month: 'long',
					day: 'numeric'
				}

				const dayName = new Date(dateSelectInfo.startStr).toLocaleDateString('fr-FR', options).split(' ')[0]

				const dayNumber = new Date(dateSelectInfo.startStr).toLocaleDateString('fr-FR', options).split(' ')[1]

				const dateMonth = new Date(dateSelectInfo.startStr).toLocaleDateString('fr-FR', options).split(' ')[1] + ' ' + new Date(dateSelectInfo.startStr).toLocaleDateString('fr-FR', options).split(' ')[2]

				document.getElementById('repeat-selector').options[2].innerHTML = 'Toutes les semaines le ' + dayName
				document.getElementById('repeat-selector').options[3].innerHTML = 'Tous les mois le ' + dayNumber
				document.getElementById('repeat-selector').options[4].innerHTML = 'Tous les ans le ' + dateMonth

				var rruleData

				document.getElementById('repeat-selector').addEventListener('change', (evt) =>
				{
					if (evt.target.selectedIndex < 1)
					{
						if (rruleData) rruleData = null
						document.getElementById('repeat-block').classList.add('is-hidden')
						document.getElementById('until-option').checked = false
						document.getElementById('count-option').checked = false
					}

					if (evt.target.selectedIndex >= 1)
					{
						rruleData = new Object()

						document.getElementById('repeat-block').classList.remove('is-hidden')
						document.getElementById('count-option').checked = true
						rruleData.count = 2
						const lendemain = new Date(dateSelectInfo.endStr.slice(0, 19))
						lendemain.setDate(lendemain.getDate() + 1)

						//note 1 : pour aider l'utilisateur on pré-remplit le champ "jusqu'à" avec la date du lendemain de la fin de l'event
						//note 2 : la date de fin d'un event allDay est déjà son lendemain
						// document.getElementById('repeat-end-date-selector').value = document.getElementById('allday-checkbox-new-event').checked ? dateSelectInfo.endStr : toISOStringWithTimezone(lendemain).slice(0, 10)
						document.getElementById('repeat-end-date-selector').value = lendemain.toISOString().slice(0, 10)

						document.getElementById('repeat-end-date-selector').setAttribute('disabled', '')
					}

					if (evt.target.selectedIndex === 1)
					{
						rruleData.freq = 'DAILY'
						rruleData.byweekday = null
					}

					if (evt.target.selectedIndex === 2)
					{
						rruleData.freq = 'WEEKLY'
						rruleData.byweekday = null
					}

					if (evt.target.selectedIndex === 3)
					{
						rruleData.freq = 'MONTHLY'
						rruleData.byweekday = null
					}

					if (evt.target.selectedIndex === 4)
					{
						rruleData.freq = 'YEARLY'
						rruleData.byweekday = null
					}

					if (evt.target.selectedIndex === 5)
					{
						rruleData.freq = 'WEEKLY'
						rruleData.byweekday = ['MO', 'TU', 'WE', 'TH', 'FR']
					}
				})

				document.getElementById('until-option').addEventListener('change', (evt) =>
				{
					if (evt.target.checked)
					{
						document.getElementById('count-option').checked = false
						rruleData.count = null
						document.getElementById('repeat-occurrence-selector').setAttribute('disabled', '')
						document.getElementById('repeat-end-date-selector').removeAttribute('disabled', '')
						// const lendemain = new Date(document.getElementById('repeat-end-date-selector').value)
						// lendemain.setDate(lendemain.getDate() + 1)
						// rruleData.until = lendemain.toISOString().slice(0, 10)

						const endDate = new Date(document.getElementById('end').value)

						const untilDate = new Date(document.getElementById('repeat-end-date-selector').value)

						untilDate.setHours(endDate.getHours())

						untilDate.setMinutes(endDate.getMinutes())

						untilDate.setSeconds(endDate.getSeconds())

						document.getElementById('allday-checkbox-new-event').checked ? rruleData.until = untilDate.toISOString().slice(0, 10) : rruleData.until = untilDate.toISOString().slice(0, 19)

					}
				})

				document.getElementById('count-option').addEventListener('change', (evt) =>
				{
					if (evt.target.checked)
					{
						document.getElementById('until-option').checked = false
						rruleData.until = null
						document.getElementById('repeat-end-date-selector').setAttribute('disabled', '')
						document.getElementById('repeat-occurrence-selector').removeAttribute('disabled', '')
						rruleData.count = document.getElementById('repeat-occurrence-selector').value
					}
				})

				document.getElementById('repeat-occurrence-selector').addEventListener('input', (evt) =>
				{
					rruleData.count = evt.target.value
					if (evt.target.checkValidity() !== true)
					{
						optimusToast("Le nombre d'occurrences doit être compris entre 2 et 999", 'is-warning')
						evt.target.classList.add('is-danger', 'is-active')
						document.getElementById('save-new-event').setAttribute('disabled', '')
					} else
					{
						evt.target.classList.remove('is-danger', 'is-active')
						document.getElementById('save-new-event').removeAttribute('disabled', '')
					}
				})

				document.getElementById('repeat-end-date-selector').addEventListener(
					'change',
					debounce((evt) =>
					{
						if (evt.target.value === '')
						{
							optimusToast('Veuillez saisir une date complète', 'is-warning')
							evt.target.classList.add('is-danger', 'is-active')
							document.getElementById('save-new-event').setAttribute('disabled', '')
						} else
						{
							evt.target.classList.remove('is-danger', 'is-active')
							document.getElementById('save-new-event').removeAttribute('disabled', '')
						}
						if (evt.target.value !== '' && evt.target.checkValidity() !== true)
						{
							optimusToast('Une limite de 5 ans ne peut pas être dépassée', 'is-warning')
							evt.target.classList.add('is-danger', 'is-active')
							document.getElementById('save-new-event').setAttribute('disabled', '')
						}

						const deb = new Date(document.getElementById('start').value)
						const endRep = new Date(evt.target.value)

						if (endRep < deb)
						{
							optimusToast("La date de fin de répétition ne peut être inférieure ou égale au début de l'événement", 'is-warning')
							evt.target.classList.add('is-danger', 'is-active')
							document.getElementById('save-new-event').setAttribute('disabled', '')
						}

						// const lendemain = new Date(evt.target.value.slice(0, 10))
						// lendemain.setDate(lendemain.getDate() + 1)
						// rruleData.until = lendemain.toISOString().slice(0, 10)


						const endDate = new Date(document.getElementById('end').value)

						const untilDate = new Date(document.getElementById('repeat-end-date-selector').value)

						untilDate.setHours(endDate.getHours())

						untilDate.setMinutes(endDate.getMinutes())

						untilDate.setSeconds(endDate.getSeconds())

						rruleData.until = document.getElementById('allday-checkbox-new-event').checked ? untilDate.toISOString().slice(0, 10) : untilDate.toISOString().slice(0, 19) + 'Z'

					}, 250)
				)

				const save_rrule = () =>
				{
					if (rruleData)
					{
						const daysArray = rruleData.byweekday
						if (daysArray)
						{
							var weekMap = daysArray.map((el) =>
							{
								return rrule.RRule[el]
							})
						}

						if (isNaN(rruleData.freq))
						{
							rruleData.freq = rrule.RRule[rruleData.freq]
						}

						rruleData.byweekday = weekMap
						const rule = new rrule.RRule(rruleData)
						const correctedString = rule.toString().slice(6)

						if (correctedString.length === 0)
						{
							return null
						} else
						{
							return correctedString
						}
					} else
					{
						return null
					}
				}

				// ****FIN RRULE****

				// const submit = document.getElementById('save-new-event')
				const form = document.getElementById('new-event-form')
				const cancel = document.getElementById('cancel-new-event')

				form.addEventListener('keypress', function (e)
				{
					if (e.key === 'Enter')
					{
						e.preventDefault()
					}
				})

				form.addEventListener('submit', (evt) =>
				{
					evt.preventDefault()

					if (document.getElementById('allday-checkbox-new-event').checked === false && document.getElementById('start').value === document.getElementById('end').value)
					{
						optimusToast("L'heure de fin doit être ultérieure à l'heure de début", 'is-warning')
						document.getElementById('end').classList.add('is-danger', 'is-active')
					} else
					{
						let alldayCheck = document.getElementById('allday-checkbox-new-event')

						let start = alldayCheck.checked ? document.getElementById('start').value : new Date(document.getElementById('start').value).toISOString().slice(0, 19).replace('T', ' ')

						const lendemain = new Date(new Date(document.getElementById('end').value).getTime() + 60 * 60 * 24 * 1000)
						const lendemainStr = lendemain.toISOString().slice(0, 10)

						let end = alldayCheck.checked ? lendemainStr : new Date(document.getElementById('end').value).toISOString().slice(0, 19).replace('T', ' ')

						const el = document.getElementById('calendar-select')

						let data = {
							title: document.getElementById('new-event-title').value,
							notes: document.getElementById('set-description').value,
							start: start,
							end: end,
							allday: alldayCheck.checked ? true : false,
							rrule: save_rrule(),
						}
						if (document.getElementById('file-finder') && document.getElementById('file-finder').dataset.id != 'null')
							data.properties = { optimus_avocats_dossier_id: document.getElementById('file-finder').dataset.id }

						rest(`${el.options[el.selectedIndex].getAttribute('data-server')}/optimus-calendar/${el.options[el.selectedIndex].getAttribute('data-owner')}/calendars/${el.options[el.selectedIndex].getAttribute('data-calendar')}/events`, 'POST', data).then(() =>
						{
							calendarInstance.refetchEvents()
							modal.close()
						})
					}
				})

				cancel.addEventListener('click', (evt) =>
				{
					evt.preventDefault()
					modal.close()
				})
			})
		})

	}

}
