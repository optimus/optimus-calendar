export default class editCalendarEvent
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
	}

	async init()
	{
		await load('/services/optimus-calendar/calendars/calendarsList.html', this.target)

		const calendarInstance = this.params

		const getCalendars = () =>
		{
			rest(`${store.user.server}/optimus-calendar/${store.user.id}/calendars?subscriptions=true`)
				.then(data =>
				{
					const calendars = data.data

					//partie souscriptions cachée faute de souscriptions
					const foundSub = calendars.find(el => el.owner)
					if (!foundSub)
						document.getElementById('shared-agendas').classList.add('is-hidden')
					//--------------------------------------------------

					let calEndpoint

					for (const calendar of calendars)
					{
						const endpointCreate = async () =>
						{
							if (calendar.owner)
							{
								const auth = await rest(`${calendar.owner.server}/optimus-base/authorizations`, 'GET', { user: store.user.id, owner: calendar.owner.id, resource: 'calendars/' + calendar.owner.calendar }).then((data) =>
								{
									const auth = data.data[0]
									return auth
								})

								if (auth.read === '0')
									calEndpoint = null
								else
									calEndpoint = `${store.user.server}/optimus-calendar/${store.user.id}/subscriptions/${calendar.id}`
							}
							else
								calEndpoint = `${store.user.server}/optimus-calendar/${store.user.id}/calendars/${calendar.id}`
						}

						endpointCreate().then(() =>
						{
							if (calEndpoint !== null)
							{
								//** construction de la liste d'affichage des calendriers

								const calendarLine = document.createElement('div')
								calendarLine.classList.add('block', 'is-flex', 'is-justify-content-space-between', 'has-background-light', 'p-2')

								const left = document.createElement('div')
								left.classList.add('is-flex')

								calendarLine.appendChild(left)

								const right = document.createElement('div')
								right.classList.add('is-flex')

								calendarLine.appendChild(right)

								const color = document.createElement('div')
								color.classList.add('icon', 'is-clickable', 'mr-2')
								color.style = 'color:#' + calendar.color

								const colorIcon = document.createElement('i')
								colorIcon.classList.add('fa-solid', 'fa-circle')

								color.appendChild(colorIcon)
								left.appendChild(color)

								const description = document.createElement('div')
								description.innerHTML = calendar.name
								left.appendChild(description)

								const editDiv = document.createElement('div')
								editDiv.classList.add('icon', 'is-clickable', 'mr-2')

								const edit = document.createElement('i')
								edit.classList.add('fa-solid', 'fa-pen', 'has-text-link')
								editDiv.appendChild(edit)

								right.appendChild(editDiv)

								const shareDiv = document.createElement('div')
								shareDiv.classList.add('icon', 'is-clickable')

								const share = document.createElement('i')
								share.classList.add('fa-solid', 'fa-share-nodes', 'has-text-link')
								shareDiv.appendChild(share)

								if (!calendar.owner)
									right.appendChild(shareDiv)

								if (calendar.owner)
									document.getElementById('shared-agendas').appendChild(calendarLine)
								else
									document.getElementById('personal-agendas').appendChild(calendarLine)

								edit.calEndpoint = calEndpoint
								edit.addEventListener('click', () =>
								{
									document.getElementById('calModal').innerHTML = ''
									const template = document.getElementById('calendarEdit')
									const clone = document.importNode(template.content, true)
									document.getElementById('calModal').appendChild(clone)

									document.getElementById('calendar-name').value = calendar.name

									document.getElementById('calendar-color').value = `#${calendar.color}`

									if (calendar.display === true)
										document.getElementById('calendar-show').checked = true
									else
										document.getElementById('calendar-show').checked = false

									document.getElementById('delete-calendar-button').addEventListener('click', () =>
									{
										document.getElementById('calModal').innerHTML = ''
										const template = document.getElementById('calendars-delete-confirmation')
										const clone = document.importNode(template.content, true)
										document.getElementById('calModal').appendChild(clone)

										document.getElementById('confirm-calendar-delete').addEventListener('click', () =>
										{
											rest(calEndpoint, 'DELETE').then(() =>
											{
												calendarInstance.refetchEvents()
												document.getElementById('calModal').innerHTML = ''
												const template = document.getElementById('calendars-list-template')
												const clone = document.importNode(template.content, true)
												document.getElementById('calModal').appendChild(clone)
												getCalendars()
											})
										})

										document.getElementById('cancel-calendar-delete').addEventListener('click', () =>
										{
											document.getElementById('calModal').innerHTML = ''
											const template = document.getElementById('calendars-list-template')
											const clone = document.importNode(template.content, true)
											document.getElementById('calModal').appendChild(clone)
											getCalendars()
										})
									})

									document.getElementById('quit-calendar-changes').addEventListener('click', () =>
									{
										document.getElementById('calModal').innerHTML = ''
										const template = document.getElementById('calendars-list-template')
										const clone = document.importNode(template.content, true)
										document.getElementById('calModal').appendChild(clone)
										getCalendars()
										document.getElementById('quit-agendas-edit').addEventListener('click', () => modal.close())
									})

									document.getElementById('save-calendar-changes').calEndpoint = this.calEndpoint
									document.getElementById('save-calendar-changes').addEventListener('click', () =>
									{
										rest(calEndpoint, 'PATCH', {
											display: document.getElementById('calendar-show').checked ? true : false,
											color: document.getElementById('calendar-color').value.slice(1),
											name: document.getElementById('calendar-name').value
										}).then(() =>
										{
											calendarInstance.refetchEvents()
											document.getElementById('calModal').innerHTML = ''
											const template = document.getElementById('calendars-list-template')
											const clone = document.importNode(template.content, true)
											document.getElementById('calModal').appendChild(clone)
											getCalendars()
											document.getElementById('quit-agendas-edit').addEventListener('click', () => modal.close())
										})

										document.getElementById('calModal').innerHTML = ''
										const template = document.getElementById('calendars-list-template')
										const clone = document.importNode(template.content, true)
										document.getElementById('calModal').appendChild(clone)
										getCalendars()
										document.getElementById('quit-agendas-edit').addEventListener('click', () => modal.close())
									})


								})

								share.addEventListener('click', () =>
								{
									// PARTAGES 
									document.getElementById('calModal').innerHTML = ''
									const template = document.getElementById('calendar-share-template')
									const clone = document.importNode(template.content, true)
									document.getElementById('calModal').appendChild(clone)

									const getShares = () =>
									{
										rest(store.user.server + '/optimus-base/authorizations', 'GET', {
											owner: store.user.id,
											resource: 'calendars/' + calendar.id
										})
											.then((data) =>
											{

												document.getElementById('share-table-body').innerHTML = ''

												const authorizations = data.data

												if (authorizations.length === 0)
												{
													const message = document.createElement('div')
													message.classList.add('message', 'is-warning', 'is-light', 'p-6')
													message.innerHTML = 'Aucun partage configuré à ce jour'
													document.getElementById('calendar-share-container').classList.remove('is-hidden')
													document.getElementById('calendar-share-container').appendChild(message)
												}
												else
												{
													document.getElementById('calendar-share-container').classList.remove('is-hidden')
													document.getElementById('calendar-share-table').classList.remove('is-hidden')
													for (const el of authorizations)
													{
														const tr = document.createElement('tr')
														document.getElementById('share-table-body').appendChild(tr)

														const user = document.createElement('td')
														tr.appendChild(user)
														user.innerHTML = el.user_displayname

														const read = document.createElement('td')
														read.classList.add('has-text-centered')
														tr.appendChild(read)
														const readBox = document.createElement('input')
														readBox.classList.add('share-box')
														readBox.name = 'read'
														readBox.type = 'checkbox'
														read.appendChild(readBox)
														if (el.read === '1') readBox.checked = true

														const edit = document.createElement('td')
														edit.classList.add('has-text-centered')
														tr.appendChild(edit)
														const editBox = document.createElement('input')
														editBox.classList.add('share-box')
														editBox.name = 'write'
														editBox.type = 'checkbox'
														edit.appendChild(editBox)
														if (el.write === '1') editBox.checked = true

														const create = document.createElement('td')
														create.classList.add('has-text-centered')
														tr.appendChild(create)
														const createBox = document.createElement('input')
														createBox.classList.add('share-box')
														createBox.name = 'create'
														createBox.type = 'checkbox'
														create.appendChild(createBox)
														if (el.create === '1') createBox.checked = true

														const del = document.createElement('td')
														del.classList.add('has-text-centered')
														tr.appendChild(del)
														const delBox = document.createElement('input')
														delBox.classList.add('share-box')
														delBox.name = 'delete'
														delBox.type = 'checkbox'
														del.appendChild(delBox)
														if (el.delete === '1') delBox.checked = true

														const trash = document.createElement('td')
														trash.classList.add('has-text-centered', 'has-text-danger')
														tr.appendChild(trash)
														const trashIcon = document.createElement('i')
														trashIcon.classList.add('fa-solid', 'fa-trash', 'is-clickable')
														trash.appendChild(trashIcon)

														document.querySelectorAll('.share-box').forEach(box =>
														{
															box.addEventListener('change', () =>
															{
																if (box.name === 'read')
																{
																	rest(store.user.server + '/optimus-base/authorizations/' + el.id, 'PATCH', {
																		read: box.checked ? '1' : '0'
																	})
																		.then(() =>
																		{
																			document.getElementById('calendar-share-selector').classList.add('is-hidden')
																			getShares()
																		})
																}
																else if (box.name === 'write')
																{
																	rest(store.user.server + '/optimus-base/authorizations/' + el.id, 'PATCH', {
																		write: box.checked ? '1' : '0'
																	})
																		.then(() =>
																		{
																			document.getElementById('calendar-share-selector').classList.add('is-hidden')
																			getShares()
																		})
																}
																else if (box.name === 'create')
																{
																	rest(store.user.server + '/optimus-base/authorizations/' + el.id, 'PATCH', {
																		create: box.checked ? '1' : '0'
																	})
																		.then(() =>
																		{
																			document.getElementById('calendar-share-selector').classList.add('is-hidden')
																			getShares()
																		})
																}
																else if (box.name === 'delete')
																{
																	rest(store.user.server + '/optimus-base/authorizations/' + el.id, 'PATCH', {
																		delete: box.checked ? '1' : '0'
																	})
																		.then(() =>
																		{
																			document.getElementById('calendar-share-selector').classList.add('is-hidden')
																			getShares()
																		})
																}
															})
														})

														trash.addEventListener('click', (evt) =>
														{
															rest(store.user.server + '/optimus-base/authorizations/' + el.id, 'DELETE')
																.then(() => getShares())
														})
													}
												}
											})
									}

									getShares()

									const addShare = () =>
									{
										if (document.getElementById('calendar-share-selector').childNodes[3]) document.getElementById('calendar-share-selector').childNodes[3].remove()

										rest(store.user.server + '/optimus-base/users', 'GET').then((data) =>
										{
											const users = data.data

											let selector = document.createElement('select')

											const opt0 = document.createElement("option")
											opt0.value = '0'

											opt0.text = ''

											selector.add(opt0, null)

											document.getElementById('calendar-share-selector').classList.remove('is-hidden')

											let selectDiv = document.createElement('div')
											selectDiv.classList.add('select', 'mb-3')

											selectDiv.appendChild(selector)

											document.getElementById('calendar-share-selector').appendChild(selectDiv)

											for (const user of users)
											{
												const opt = document.createElement("option")
												opt.value = user.id
												opt.text = user.displayname
												selector.add(opt, null)
											}

											selector.addEventListener('change', (evt) =>
											{
												const selectedUser = evt.target.options[evt.target.selectedIndex].value

												if (evt.target.selectedIndex.value !== '0')
												{
													rest(store.user.server + '/optimus-base/authorizations', 'POST', {
														owner: store.user.id,
														user: selectedUser,
														resource: 'calendars/' + calendar.id,
														read: '0',
														write: '0',
														create: '0',
														delete: '0'
													})
														.then(() =>
														{
															document.getElementById('calendar-share-selector').classList.add('is-hidden')
															document.getElementById('calendar-share-selector').childNodes[3].remove()
															getShares()
														})
												}
											})
										})
									}

									document.getElementById('add-share').addEventListener('click', addShare)

									document.getElementById('quit-calendar-share').addEventListener('click', () =>
									{
										document.getElementById('calModal').innerHTML = ''
										const template = document.getElementById('calendars-list-template')
										const clone = document.importNode(template.content, true)
										document.getElementById('calModal').appendChild(clone)
										getCalendars()
										document.getElementById('quit-agendas-edit').addEventListener('click', () => modal.close())
									})
								})

								document.getElementById('create-agenda').addEventListener('click', () =>
								{
									document.getElementById('calModal').innerHTML = ''
									const template = document.getElementById('calendar-creation')
									const clone = document.importNode(template.content, true)
									document.getElementById('calModal').appendChild(clone)
									document.getElementById('save-new-calendar').addEventListener('click', (evt) =>
									{
										const endP = `${store.user.server}/optimus-calendar/${store.user.id}/calendars`
										rest(endP, 'POST', {
											name: document.getElementById('calendar-name').value,
											color: document.getElementById('calendar-color').value.slice(1),
											display: document.getElementById('calendar-show').checked ? true : false
										}).then(() =>
										{
											document.getElementById('calModal').innerHTML = ''
											const template = document.getElementById('calendars-list-template')
											const clone = document.importNode(template.content, true)
											document.getElementById('calModal').appendChild(clone)
											getCalendars()
										})
									})

									document.getElementById('cancel-calendar-creation').addEventListener('click', () =>
									{
										document.getElementById('calModal').innerHTML = ''
										const template = document.getElementById('calendars-list-template')
										const clone = document.importNode(template.content, true)
										document.getElementById('calModal').appendChild(clone)
										document.getElementById('quit-agendas-edit').addEventListener('click', () => modal.close())
										getCalendars()
									})
								})
							}
						})
					}
				})
		}

		getCalendars()

		document.getElementById('quit-agendas-edit').addEventListener('click', () => modal.close())
	}

}
