export default class calendarIndex
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
	}

	async init()
	{
		await load('/components/optimus_calendar/index.js', this.target)
			.then(component =>
			{
				let optimusCalendar = component.fullcalendar

				const defaultSource = () =>
				{
					const currentSource = optimusCalendar.getEventSources()

					for (const source of currentSource)
					{
						source.remove()
					}

					optimusCalendar.addEventSource(function (info, successCallback, failureCallback)
					{
						const calendarPromises = new Array()

						rest(store.user.server + '/optimus-calendar/' + store.user.id + '/calendars?subscriptions=true')
							.then((data) =>
							{
								const calendars = data.data

								let endpoint

								const createEndpoints = async () =>
								{

									for (const calendar of calendars)
									{
										if (calendar.owner)
										{
											const auth = await rest(`${calendar.owner.server}/optimus-base/authorizations`, 'GET', { user: store.user.id, owner: calendar.owner.id, resource: 'calendars/' + calendar.owner.calendar }).then((data) =>
											{
												const auth = data.data[0]
												return auth
											})

											if (auth && auth.read === '0')
											{
												endpoint = null
											} else if (auth)
											{
												endpoint = `${calendar.owner.server}/optimus-calendar/${calendar.owner.id}/calendars/${calendar.owner.calendar}`
											}

										} else
										{
											endpoint = `${store.user.server}/optimus-calendar/${store.user.id}/calendars/${calendar.id}`
										}

										if (calendar.display === true && endpoint !== null)
										{
											let newPromise = rest(endpoint + '/events', 'GET', {
												start: info.startStr.slice(0, 10),
												end: info.endStr.slice(0, 10)
											}).then((data) =>
											{
												if (data && data.data)
												{
													data.data.map((el) => (el.calColor = calendar.color))
													data.data.map((el) => (el.calName = calendar.displayname))
													if (calendar.owner)
													{
														data.data.map((el) => (el.owner = calendar.owner.id))
														data.data.map((el) => (el.server = calendar.owner.server))
													} else
													{
														data.data.map((el) => (el.owner = store.user.id))
														data.data.map((el) => (el.server = store.user.server))
													}
													return data
												}
											})
											calendarPromises.push(newPromise)
										}
									}
								}

								createEndpoints().then(() =>
								{
									Promise.all(calendarPromises).then((values) =>
									{
										let myEvents = new Array()
										for (const el of values)
										{
											if (el && el.data)
											{
												for (const event of el.data)
												{
													event.backgroundColor = '#' + event.calColor
													event.borderColor = '#' + event.calColor
													event.allDay = event.allday
													delete event.allday

													if (event.allDay)
													{
														event.start = event.start + 'T00:00:00'
													}

													if (event.rrule)
													{
														const rule = rrule.RRule.fromString(event.rrule)

														if (rule.origOptions.byweekday)
														{
															const mappedWeek = rule.origOptions.byweekday.map((element) =>
															{
																return element.weekday
															})
															delete rule.origOptions.byweekday
															rule.origOptions.byweekday = mappedWeek
														}

														event.rrule = rule.origOptions
														const dT = new Date(event.start).toISOString().slice(0, 19) + 'Z'
														event.rrule.dtstart = dT

														delete event.end
													}
													else
														event.duration = null

													if (event.exdate)
														event.exdate = event.exdate.slice(7).split(',')

													myEvents.push(event)
												}
											}
										}
										successCallback(myEvents)
									})

								})
							})

							.catch((err) =>
							{
								failureCallback(err)
							})
					})
				}

				defaultSource()

				let didSelect = false

				optimusCalendar.on('dateClick', function (info)
				{
					//** Déclenchement  */

					if (!didSelect)
					{
						modal.open('/services/optimus-calendar/calendars/newEvent.js', false, [info, optimusCalendar])
					}
					didSelect = false
				})

				document.querySelectorAll('.optimus-calendar-addevent-button').forEach((item) =>
				{
					item.addEventListener('click', () =>
					{

						const info = new Object()
						info.date = new Date()
						info.date.setMinutes(info.date.getMinutes() + 30)
						info.date.setMinutes(0)
						info.date.setSeconds(0)
						info.dateStr = info.date.toISOString().slice(0, 16)
						info.allDay = false
						modal.open('/services/optimus-calendar/calendars/newEvent.js', false, [info, optimusCalendar])
					})
				})
				document.querySelector('.optimus-calendar-dropdown-addEvent-trigger').addEventListener('mousedown', () => document.querySelector('.optimus-calendar-addevent-button').click())

				optimusCalendar.on('select', function (selectionInfo)
				{
					// selectionInfo.jsEvent.stopPropagation()
					didSelect = true
					modal.open('/services/optimus-calendar/calendars/newEventBySelection.js', false, [selectionInfo, optimusCalendar])
				})

				optimusCalendar.on('eventClick', function (info)
				{
					modal.open('/services/optimus-calendar/calendars/editEvent.js', false, [info, optimusCalendar])
				})


				const dragOrResize = function (info)
				{

					if (info.oldEvent._def.recurringDef)
					{
						var rule = new rrule.RRule(info.oldEvent._def.recurringDef.typeData.rruleSet._rrule[0].origOptions)

						const allOccs = rule.all()

						const movedOcc = allOccs.find(el => el.toISOString().slice(0, 10) === info.oldEvent.start.toISOString().slice(0, 10))

						const movedOccPosition = allOccs.indexOf(movedOcc)

						var passedCount = movedOccPosition

						const remainingCount = allOccs.length - passedCount

						var before = rule.before(info.oldEvent.start)

						modal.open('/services/optimus-calendar/calendars/dragEventPrompt.html', false)
							.then(() =>
							{
								const editStart = info.event.allDay ? info.event.startStr : info.event.start.toISOString().slice(0, 19).replace('T', ' ')

								const lendemain = new Date(info.event.end.getTime() + 60 * 60 * 24 * 1000)
								const lendemainStr = lendemain.toISOString().slice(0, 10)

								const editEnd = info.event.allDay ? lendemainStr : info.event.end.toISOString().slice(0, 19).replace('T', ' ')

								document.getElementById('cancel-edit').addEventListener('click', () =>
								{
									optimusCalendar.refetchEvents()
									modal.close()
								})

								document.getElementById('edit-one').addEventListener('click', () =>
								{
									let deletedDate = info.oldEvent.allDay ? info.oldEvent.startStr : info.oldEvent.start.toISOString()

									rest(`${info.oldEvent._def.extendedProps.server}/optimus-calendar/${info.oldEvent._def.extendedProps.owner}/calendars/${info.oldEvent._def.extendedProps.calendar}/events/${info.event.id}`, 'PATCH', {
										exdate: deletedDate
									}).then(() =>
									{
										rest(`${info.oldEvent._def.extendedProps.server}/optimus-calendar/${info.oldEvent._def.extendedProps.owner}/calendars/${info.oldEvent._def.extendedProps.calendar}/events`, 'POST', {

											start: editStart,
											end: editEnd,
											allday: info.event.allDay ? true : false,
											rrule: null
										}).then(() =>
										{
											optimusCalendar.refetchEvents()
											modal.close()
										})
									})

								})

								document.getElementById('edit-next').addEventListener('click', () =>
								{
									// ** 1) Modifier l'event existant pour réduire until ou count - pour ne garder que les occurrences passées (qui ne doivent pas comprendre la date cliquée !)
									// ** 2) Puis créer un event qui part de la date cliquée avec soit un count = count initial - count passés, soit un until décalé

									let existingRule = info.oldEvent._def.recurringDef.typeData.rruleSet._rrule[0].origOptions

									delete (existingRule.dtstart)

									if (info.event._def.recurringDef.typeData.rruleSet._rrule[0].origOptions.until)
									{
										if (passedCount === 0)				
										{
											// if (info.event.start > info.oldEvent._def.recurringDef.typeData.rruleSet._rrule[0].origOptions.until)
											// {
											const endDate = new Date(info.event.start)
											endDate.setDate(info.event.start.getDate() + (remainingCount - 1))
											endDate.setHours(info.event.end.getHours())
											endDate.setMinutes(info.event.end.getMinutes())
											endDate.setSeconds(info.event.end.getSeconds())
											existingRule.until = endDate
											const rule = new rrule.RRule(existingRule)
											const ruleStr = rule.toString().slice(6)

											rest(`${info.oldEvent._def.extendedProps.server}/optimus-calendar/${info.oldEvent._def.extendedProps.owner}/calendars/${info.oldEvent._def.extendedProps.calendar}/events/${info.event.id}`, 'PATCH', {
												start: editStart,
												end: editEnd,
												allday: info.event.allDay ? true : false,
												rrule: ruleStr
											}).then(() =>
											{
												optimusCalendar.refetchEvents()
												modal.close()
												optimusToast('Événement enregistré', 'is-dark')
											})
											// } else
											// {
											// 	rest(`${info.oldEvent._def.extendedProps.server}/optimus-calendar/${info.oldEvent._def.extendedProps.owner}/calendars/${info.oldEvent._def.extendedProps.calendar}/events/${info.event.id}`, 'PATCH', {
											// 		start: editStart,
											// 		end: editEnd,
											// 		allday: info.event.allDay ? true : false,
											// 	}).then(() =>
											// 	{
											// 		optimusCalendar.refetchEvents()
											// 		modal.close()
											// 		optimusToast('Événement enregistré', 'is-dark')
											// 	})
											// }

										} else
										{
											existingRule.until = before

											const rule = new rrule.RRule(existingRule)

											let ruleStr = rule.toString().slice(6)

											rest(`${info.oldEvent._def.extendedProps.server}/optimus-calendar/${info.oldEvent._def.extendedProps.owner}/calendars/${info.oldEvent._def.extendedProps.calendar}/events/${info.event.id}`, 'PATCH', {
												rrule: passedCount === 1 ? null : ruleStr
											}).then(() =>
											{
												const endDate = new Date(info.event.start)
												endDate.setDate(info.event.start.getDate() + (remainingCount - 1))
												endDate.setHours(info.event.end.getHours())
												endDate.setMinutes(info.event.end.getMinutes())
												endDate.setSeconds(info.event.end.getSeconds())
												existingRule.until = endDate
												const rule2 = new rrule.RRule(existingRule)
												const ruleStr2 = rule2.toString().slice(6)

												rest(`${info.oldEvent._def.extendedProps.server}/optimus-calendar/${info.oldEvent._def.extendedProps.owner}/calendars/${info.oldEvent._def.extendedProps.calendar}/events`, 'POST', {
													title: info.oldEvent._def.title,
													notes: info.oldEvent.extendedProps.notes,
													start: editStart,
													end: editEnd,
													allday: info.event.allDay ? true : false,
													rrule: ruleStr2
												}).then(() =>
												{
													optimusCalendar.refetchEvents()
													modal.close()
													optimusToast('Événement enregistré', 'is-dark')
												})
											})

										}
									} else if (info.event._def.recurringDef.typeData.rruleSet._rrule[0].origOptions.count)
									{
										if (passedCount === 0)				
										{
											rest(`${info.oldEvent._def.extendedProps.server}/optimus-calendar/${info.oldEvent._def.extendedProps.owner}/calendars/${info.oldEvent._def.extendedProps.calendar}/events/${info.event.id}`, 'PATCH', {
												start: editStart,
												end: editEnd,
												allday: info.event.allDay ? true : false,
											}).then(() =>
											{
												optimusCalendar.refetchEvents()
												modal.close()
												optimusToast('Événement enregistré', 'is-dark')
											})

										} else
										{
											existingRule.count = passedCount

											const rule = new rrule.RRule(existingRule)
											const ruleStr = rule.toString().slice(6)

											rest(`${info.oldEvent._def.extendedProps.server}/optimus-calendar/${info.oldEvent._def.extendedProps.owner}/calendars/${info.oldEvent._def.extendedProps.calendar}/events/${info.event.id}`, 'PATCH', {
												rrule: passedCount === 1 ? null : ruleStr
											}).then(() =>
											{
												existingRule.count = remainingCount
												const rule = new rrule.RRule(existingRule)
												const ruleStr = rule.toString().slice(6)

												rest(`${info.oldEvent._def.extendedProps.server}/optimus-calendar/${info.oldEvent._def.extendedProps.owner}/calendars/${info.oldEvent._def.extendedProps.calendar}/events`, 'POST', {
													title: info.oldEvent._def.title,
													notes: info.oldEvent.extendedProps.notes,
													start: editStart,
													end: editEnd,
													allday: info.event.allDay ? true : false,
													rrule: ruleStr
												}).then(() =>
												{
													optimusCalendar.refetchEvents()
													modal.close()
													optimusToast('Événement enregistré', 'is-dark')
												})
											})
										}
									}
								})
							})

					}
					else
					{
						const editStart = info.event.allDay ? info.event.startStr.slice(0, 10).replace('T', ' ') : info.event.start.toISOString().slice(0, 19).replace('T', ' ')

						const lendemain = new Date(info.event.end.getTime() + 60 * 60 * 24 * 1000)
						const lendemainStr = lendemain.toISOString().slice(0, 10)

						const editEnd = info.event.allDay ? lendemainStr : info.event.end.toISOString().slice(0, 19).replace('T', ' ')

						rest(`${info.oldEvent._def.extendedProps.server}/optimus-calendar/${info.oldEvent._def.extendedProps.owner}/calendars/${info.oldEvent._def.extendedProps.calendar}/events/${info.event.id}`, 'PATCH', {
							start: editStart,
							end: editEnd,
							allday: info.event.allDay ? true : false,
							rrule: null
						}).then(() =>
						{
							optimusCalendar.refetchEvents()
							modal.close()
							optimusToast('Événement enregistré', 'is-dark')
						})
					}
				}

				optimusCalendar.on('eventDrop', function (eventDropInfo)
				{
					dragOrResize(eventDropInfo)
				})


				optimusCalendar.on('eventResize', function (eventResizeInfo)
				{
					dragOrResize(eventResizeInfo)
				})

				optimusCalendar.render()

				document.querySelectorAll('.optimus-calendar-agendas-button').forEach((item) =>
				{
					item.addEventListener('mousedown', () =>
					{
						modal.open('/services/optimus-calendar/calendar/calendar_list_modal.js', false, optimusCalendar)
					})
				})

				const eventModal = (el) =>
				{
					el.classList.add('is-active')
				}

				document.querySelectorAll('.optimus-calendar-search-button').forEach((item) =>
				{
					item.addEventListener('mousedown', (e) =>
					{
						const currentSource = optimusCalendar.getEventSources()

						for (const source of currentSource)
						{
							source.remove()
						}

						optimusCalendar.changeView('listYear')

						document.querySelector('.optimus-calendar-header').classList.add('is-hidden')

						document.getElementById('optimus-calendar-search-bar').classList.remove('is-hidden')

						document.getElementById('quit-search').classList.remove('is-hidden')

						document.getElementById('delete-current-search').addEventListener('click', () =>
						{
							document.getElementById('event-search').value = ''
							const currentSource = optimusCalendar.getEventSources()

							for (const source of currentSource)
								source.remove()
						})

						window.setTimeout(function ()
						{
							document.getElementById('event-search').focus()
						}, 0)

						optimusCalendar.eventSearch = (words) =>
						{
							if (words.length === 0)
							{
								const currentSource = optimusCalendar.getEventSources()

								for (const source of currentSource)
								{
									source.remove()
								}
								return
							} else
							{
								const currentSource = optimusCalendar.getEventSources()

								for (const source of currentSource)
								{
									source.remove()
								}
								optimusCalendar.addEventSource(function (info, successCallback, failureCallback)
								{
									const calendarPromises = new Array()

									rest(store.user.server + '/optimus-calendar/' + store.user.id + '/calendars?subscriptions=true', 'GET', {})
										.then((data) =>
										{
											const calendars = data.data

											let endpoint

											const createEndpoints = async () =>
											{

												for (const calendar of calendars)
												{
													if (calendar.owner)
													{
														const auth = await rest(`${calendar.owner.server}/optimus-base/authorizations`, 'GET', { user: store.user.id, owner: calendar.owner.id, resource: 'calendars/' + calendar.owner.calendar }).then((data) =>
														{
															const auth = data.data[0]
															return auth
														})

														if (auth.read === '0')
														{
															endpoint = null
														} else
														{
															endpoint = `${calendar.owner.server}/optimus-calendar/${calendar.owner.id}/calendars/${calendar.owner.calendar}`
														}
													} else
													{
														endpoint = `${store.user.server}/optimus-calendar/${store.user.id}/calendars/${calendar.id}`
													}

													if (calendar.display === true && endpoint !== null)
													{
														let newPromise = rest(endpoint + '/events', 'GET', {
															start: info.startStr.slice(0, 10),
															end: info.endStr.slice(0, 10),
															search: words
														}).then((data) =>
														{
															if (data && data.data)
															{
																data.data.map((el) => (el.calColor = calendar.color))
																data.data.map((el) => (el.calName = calendar.displayname))
																if (calendar.owner)
																{
																	data.data.map((el) => (el.owner = calendar.owner.id))
																	data.data.map((el) => (el.server = calendar.owner.server))
																} else
																{
																	data.data.map((el) => (el.owner = store.user.id))
																	data.data.map((el) => (el.server = store.user.server))
																}
																return data
															}
														})
														calendarPromises.push(newPromise)
													}
												}
											}

											createEndpoints().then(() =>
											{
												Promise.all(calendarPromises).then((values) =>
												{
													let myEvents = new Array()
													for (const el of values)
													{
														if (el && el.data)
														{
															for (const event of el.data)
															{
																event.backgroundColor = '#' + event.calColor
																event.borderColor = '#' + event.calColor
																event.allDay = event.allday
																delete event.allday

																if (event.allDay)
																{
																	event.start = event.start + 'T00:00:00'
																}

																if (event.rrule)
																{
																	const rule = rrule.RRule.fromString(event.rrule)

																	if (rule.origOptions.byweekday)
																	{
																		const mappedWeek = rule.origOptions.byweekday.map((element) =>
																		{
																			return element.weekday
																		})
																		delete rule.origOptions.byweekday
																		rule.origOptions.byweekday = mappedWeek
																	}

																	event.rrule = rule.origOptions
																	const dT = new Date(event.start).toISOString().slice(0, 19) + 'Z'
																	event.rrule.dtstart = dT
																	delete event.end
																} else
																{
																	event.duration = null
																}

																if (event.exdate)
																{
																	event.exdate = event.exdate.slice(7).split(',')
																}

																myEvents.push(event)
															}
														}
													}

													successCallback(myEvents)
												})

											})
										})

										.catch((err) =>
										{
											failureCallback(err)
										})
								})
							}
						}

						let timeout = null

						document.getElementById('event-search').addEventListener('keyup', (e) =>
						{
							clearTimeout(timeout)

							timeout = setTimeout(function ()
							{
								optimusCalendar.eventSearch(e.target.value)
							}, 200)
						})

						document.getElementById('quit-search').addEventListener('click', () =>
						{
							const lastView = store.calendarView

							const container = document.querySelector('.optimus-calendar-header')

							container.querySelectorAll('.optimus-calendar-changeview-buttons .button').forEach((button) =>
							{
								button.classList.remove('is-link')
							})
							container.querySelector('.optimus-calendar-' + lastView + '-button').classList.add('is-link')
							optimusCalendar.changeView(lastView)

							defaultSource()

							document.getElementById('event-search').value = ''
							document.getElementById('optimus-calendar-search-bar').classList.add('is-hidden')

							document.getElementById('quit-search').classList.add('is-hidden')
							document.querySelector('.optimus-calendar-header').classList.remove('is-hidden')
						})
					})
				})


				// optimusCalendar.render()

			})
	}
}


