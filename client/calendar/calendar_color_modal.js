export default class OptimusCalendarListModal
{
	constructor(target, params) 
	{
		this.target = target
		this.calendar = params
	}

	async init()
	{
		await load('/services/optimus-calendar/calendar/calendar_color_modal.html', this.target)

		modal.querySelector('#calendar-color').value = '#' + this.calendar.color

		for (let rect of modal.querySelectorAll('rect'))
			rect.onmouseup = () =>
				rest(store.user.server + '/optimus-calendar/' + store.user.id + '/calendars/' + this.calendar.id, 'PATCH', { color: rect.getAttribute('fill').substring(1) })
					.then(response => 
					{
						if (response.code == 200)
						{
							this.calendar.calendarInstance.refetchEvents()
							modal.open('/services/optimus-calendar/calendar/calendar_list_modal.js', false, this.calendar.calendarInstance, false)
						}
					})


		document.getElementById('calendar-color').onchange = () =>
			rest(store.user.server + '/optimus-calendar/' + store.user.id + '/calendars/' + this.calendar.id, 'PATCH', { color: modal.querySelector('#calendar-color').value.substring(1) })
				.then(response => 
				{
					if (response.code == 200)
					{
						this.calendar.calendarInstance.refetchEvents()
						modal.open('/services/optimus-calendar/calendar/calendar_list_modal.js', false, this.calendar.calendarInstance, false)
					}
				})

		modal.querySelector('.modal-card-close').onclick = () => modal.open('/services/optimus-calendar/calendar/calendar_list_modal.js', false, this.calendar.calendarInstance, false)
	}
}