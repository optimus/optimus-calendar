export default class OptimusCalendarEventModalGeneral
{
	constructor(target, params) 
	{
		this.target = target
		this.calendar = params.calendar
		this.event = params.event
		this.endpoint = params.endpoint
		this.calendarInstance = params.calendarInstance
	}

	async init()
	{
		await load('/services/optimus-calendar/calendar/event_modal_general.html', this.target)

		let event = (await rest(this.endpoint)).data

		//TITLE
		this.target.querySelector('#title').placeholder = 'Evènement N°' + event.id
		this.target.querySelector('#title').dataset.endpoint = this.endpoint

		//ALLDAY
		this.target.querySelector('#allday').dataset.endpoint = this.endpoint


		//DATEPICKER START
		let start_datepicker = datepicker('#start_datepicker',
			{
				type: event.allday ? 'date' : 'datetime',
				showClearButton: false,
				startDate: new Date(this.event.start),
				startTime: new Date(this.event.start)
			})
		this.target.querySelector('#start').type = event.allday ? 'date' : 'datetime-local'
		this.target.querySelector('#start').value = event.allday ? new Date(this.event.start).toISOString().split('T')[0] : new Date(this.event.start).toISOString().split('Z')[0]
		this.target.querySelector('#start').onclick = event => event.preventDefault()
		this.target.querySelector('#start').nextElementSibling.onclick = () =>
		{
			this.target.querySelector('#start_datepicker_block').classList.remove('is-hidden')
			setTimeout(() => start_datepicker.show(), 10)
		}
		start_datepicker.on('select', info => 
		{
			//console.log(info)
			//console.log(new Date(info.data.date.start).toISOString())
			//console.log(event.allday ? new Date(info.timeStamp).toISOString().split('T')[0] : new Date(info.timeStamp).toISOString())
			//this.target.querySelector('#start_datepicker_block').classList.add('is-hidden')
			//this.target.querySelector('#start').value = event.allday ? new Date(start_datepicker.value()).toISOString().split('T')[0] : new Date(start_datepicker.value()).toISOString()
			//this.target.querySelector('#start').value = new Date(info.timeStamp).toISOString()
		})
		start_datepicker.on('hide', info => 
		{
			console.log(new Date(info.data.date.start).toISOString())
			//console.log(new Date(info.data.date.start).toISOString())
			//console.log(event.allday ? new Date(info.timeStamp).toISOString().split('T')[0] : new Date(info.timeStamp).toISOString())
			//this.target.querySelector('#start_datepicker_block').classList.add('is-hidden')
			//this.target.querySelector('#start').value = event.allday ? new Date(start_datepicker.value()).toISOString().split('T')[0] : new Date(start_datepicker.value()).toISOString()
			//this.target.querySelector('#start').value = new Date(info.timeStamp).toISOString()
		})

		let optimusForm = await load('/components/optimus_form.js', this.target, event)


		return

		let start_datepicke = datepicker('start_datepicker',
			{
				type: event.allday ? 'date' : 'datetime',
				showClearButton: false,
				startDate: new Date(this.event.start),
				startTime: new Date(this.event.start)
			})
		this.target.querySelector('#start_datepicker_button').onclick = () => 
		{
			this.target.querySelector('#start_datepicker_block > div').classList.remove('is-hidden')
			setTimeout(() => start_datepicker.show(), 10)
		}

		start_datepicker.on('hide', info =>
		{
			this.target.querySelector('#start_datepicker_block > div').classList.add('is-hidden')
			start_datepicker.type = 'date'

		})
		this.target.querySelector('#start').value = new Date(this.event.start)
	}
}