export default class OptimusCalendarEventModalGeneralNotes
{
	constructor(target, params) 
	{
		this.target = target
		this.endpoint = params.endpoint
	}

	async init()
	{
		await load('/services/optimus-calendar/calendar/event_modal_notes.html', this.target)
		let event = (await rest(this.endpoint)).data
		this.target.querySelector('#notes').dataset.endpoint = this.endpoint
		let optimusForm = await load('/components/optimus_form.js', this.target, event)
	}
}