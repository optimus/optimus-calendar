export default class OptimusCalendarShareModal
{
	constructor(target, params) 
	{
		this.target = target
		this.calendar = params
	}

	async init()
	{
		let component = this

		await load('/services/optimus-calendar/calendar/calendar_share_modal.html', this.target)

		let myauth = await load('/components/optimus_authorizations/index.js', modal.querySelector('.modal-card-body'),
			{
				server: store.user.server,
				url: store.user.server + '/optimus-base/authorizations',

				showTitle: false,
				title: null,
				showSubtitle: false,
				subtitle: null,
				showAddButton: 'bottom',
				pagination: false,

				striped: true,
				bordered: true,
				columnSeparator: true,

				columns: ['user', 'read', 'write', 'delete'],
				headerFilterInputs: null,

				filters:
				{
					owner: store.user.id,
					resource: 'calendars/' + this.calendar.id
				},

				editor:
				{
					blocks: ['user', 'rights'],
					read: true,
					write: false,
					create: false,
					delete: false,
					onCreated: () => modal.open('/services/optimus-calendar/calendar/calendar_share_modal.js', false, this.calendar, false),
					onCancel: () => modal.open('/services/optimus-calendar/calendar/calendar_share_modal.js', false, this.calendar, false),
				}
			})
		myauth.tabulator.element.querySelector('.tabulator-header').style.filter = 'grayscale(0.9)'
		setTimeout(() => myauth.tabulator.redraw(), 50)

		modal.querySelector('.modal-card-close').onclick = () => modal.open('/services/optimus-calendar/calendar/calendar_list_modal.js', false, this.calendar.calendarInstance, false)
	}
}