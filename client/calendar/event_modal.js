export default class OptimusCalendarEventEditorModal
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
	}

	async init()
	{
		await load('/services/optimus-calendar/calendar/event_modal.html', this.target)

		let tabs =
			[
				{
					id: "tab_general",
					text: "Général",
					link: "/services/optimus-calendar/calendar/event_modal_general.js",
					default: true,
					position: 100
				},
				{
					id: "tab_notes",
					text: "Notes",
					link: "/services/optimus-calendar/calendar/event_modal_notes.js",
					position: 200
				},
				{
					id: "tab_recurrence",
					text: "Récurrence",
					link: "/services/optimus-calendar/calendar/event_modal_recurrence.js",
					position: 300
				}
			]

		let owner_services = (this.owner != store.user.id) ? await rest(store.user.server + '/optimus-base/users/' + this.owner + '/services').then(response => response.data.map(service => service.name)) : store.services.map(service => service.name)
		store.services.map(service => 
		{
			if (owner_services.includes(service.name) && service.optimus_calendar_event_tabs)
				tabs = tabs.concat(service.optimus_calendar_event_tabs)
		})

		this.params.endpoint = (this.params.calendar.owner ? this.params.calendar.owner.server : store.user.server) + '/optimus-calendar/' + (this.params.calendar.owner ? this.params.calendar.owner.id : store.user.id) + '/calendars/' + (this.params.calendar.owner ? this.params.calendar.owner.calendar : this.params.event.calendar) + '/events/' + this.params.event.id


		await load('/components/optimus_tabs.js', this.target,
			{
				tabs_container: document.getElementById('event_tabs'),
				content_container: document.getElementById('event_tabs_container'),
				tabs: tabs,
				params: this.params
			})

		modal.querySelector('.modal-card-close').onclick = () => modal.close()

	}
}