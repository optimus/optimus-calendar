export default class OptimusCalendarListModal
{
	constructor(target, params) 
	{
		this.target = target
		this.calendarInstance = params
	}

	async init()
	{
		await load('/services/optimus-calendar/calendar/calendar_list_modal.html', this.target)

		await rest(store.user.server + '/optimus-calendar/' + store.user.id + '/calendars?subscriptions=true')
			.then(calendars =>
			{
				if (calendars.data.length == 0)
					return false

				calendars.data.sort((a, b) => (a.owner ? a.owner.id : 0) - (b.owner ? b.owner.id : 0))
				let current_owner = store.user.id

				for (const calendar of calendars.data)
				{
					if (calendar?.owner?.id != current_owner)
					{
						const title = document.createElement('div')
						title.classList.add('subtitle', 'p-0', 'mb-0')
						if (current_owner != store.user.id)
							title.classList.add('mt-5')
						title.innerText = current_owner == !calendar.owner ? 'Mes agendas' : calendar.owner?.displayname
						modal.querySelector('#calendars').appendChild(title)

						const div = document.createElement('div')
						modal.querySelector('#calendars').appendChild(div)

						current_owner = calendar?.owner?.id
					}

					const template = document.getElementById('optimus_calendar_row').content.cloneNode(true)
					template.querySelector('.calendar-name').value = calendar.displayname
					template.querySelector('.calendar-color').style.color = '#' + calendar.color
					template.querySelector('input').id = 'calendar_switch_' + calendar.id
					if (!calendar.display)
						template.querySelector('.view-button > i').classList.add('fa-eye-slash')

					calendar.calendarInstance = this.calendarInstance

					if (calendar.owner)
					{
						template.querySelector('.share-button').classList.add('is-invisible')
						template.querySelector('.delete-button').classList.add('is-invisible')
					}
					else
					{
						template.querySelector('.share-button').onclick = () => modal.open('/services/optimus-calendar/calendar/calendar_share_modal.js', false, calendar, false)
						template.querySelector('.delete-button').onclick = () => modal.open('/services/optimus-calendar/calendar/calendar_delete_modal.js', false, calendar, false)
					}

					template.querySelector('.view-button').onclick = function () 
					{
						rest(store.user.server + '/optimus-calendar/' + store.user.id + '/calendars/' + calendar.id, 'PATCH', { display: this.querySelector('i').classList.contains('fa-eye-slash') })
							.then(response => 
							{
								if (response.code == 200)
								{
									this.querySelector('i').classList.toggle('fa-eye-slash')
									calendar.calendarInstance.refetchEvents()
								}
							})
					}

					template.querySelector('.calendar-name').onchange = function () { rest(store.user.server + '/optimus-calendar/' + store.user.id + '/calendars/' + calendar.id, 'PATCH', { name: this.value }) }
					template.querySelector('.calendar-name').onkeyup = function (event) { event.key == 'Enter' && this.blur() }

					template.querySelector('.calendar-color').onclick = () => modal.open('/services/optimus-calendar/calendar/calendar_color_modal.js', false, calendar, false)

					modal.querySelector('#calendars > div:last-of-type').appendChild(template)

				}
			})

		modal.querySelector('.close-button').onclick = () => modal.close()
		modal.querySelector('.modal-card-close').onclick = () => modal.close()

		modal.querySelector('.create-button').onclick = () =>
			rest(store.user.server + '/optimus-calendar/' + store.user.id + '/calendars', 'POST')
				.then(response => response.code == 201 && modal.open('/services/optimus-calendar/calendar/calendar_list_modal.js', false, this.calendarInstance, false)
					.then(() => this.calendarInstance && this.calendarInstance.refetchEvents()))
	}
}