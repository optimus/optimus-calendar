export default class OptimusCalendarDeleteModal
{
	constructor(target, params) 
	{
		this.target = target
		this.calendar = params
	}

	async init()
	{
		await load('/services/optimus-calendar/calendar/calendar_delete_modal.html', this.target)

		modal.querySelector('.cancel-button').onclick = () => modal.open('/services/optimus-calendar/calendar/calendar_list_modal.js', false, this.calendar.calendarInstance, false)

		modal.querySelector('.confirm-button').onclick = () => 
		{
			rest(store.user.server + '/optimus-calendar/' + store.user.id + '/calendars/' + this.calendar.id, 'DELETE')
				.then(response => 
				{
					if (response.code == 200)
					{
						modal.open('/services/optimus-calendar/calendar/calendar_list_modal.js', false, this.calendar.calendarInstance, false)
						this.calendar.calendarInstance.refetchEvents()
					}
				})
		}
	}
}
